#------------------------------------------------------------------------------
# Viewpoint Quality Lib
#
# Copyright (c) 2017-2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
#------------------------------------------------------------------------------
#                                 License
#
# Licensed under the 3-Clause BSD License (the "License");
# you may not use this file except in compliance with the License.
# See the file LICENSE for the full text.
# You may obtain a copy of the License at
#
#     https://opensource.org/licenses/BSD-3-Clause
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------------------------

from conans import ConanFile, CMake

class ViewpointQuality(ConanFile):
    name = "vq"
    version = "0.2"
    license = "3-Clause BSD License"
    description = """Viewpoint Quality Lib"""
    settings = "os", "compiler", "build_type", "arch"

    requires = (("catch/1.9.7@RWTH-VR/thirdparty"),
                ("cppcheck/1.82@RWTH-VR/thirdparty"),
                ("cpplint/9883c51@RWTH-VR/thirdparty"),
                ("gl/1.0.0@RWTH-VR/thirdparty"),
                ("glew/2.1.0_1@RWTH-VR/thirdparty"),
                ("glm/0.9.8@RWTH-VR/thirdparty"),
				("zlib/1.2.8@RWTH-VR/thirdparty"),
                ("SDL2/2.0.5@RWTH-VR/thirdparty"))
    generators = "cmake"

    def configure(self):
       self.options["glew"].shared = True
       self.options["gl"].shared = False

    def imports(self):
       self.copy("*.dll", dst="lib", src="bin")
       self.copy("*.dll", dst="tests/Debug", src="bin")
       self.copy("*.dll", dst="tests/Release", src="bin")
       self.copy("*.dll", dst="demos/viewer/Debug", src="bin")
       self.copy("*.dll", dst="demos/viewer/Release", src="bin")
       self.copy("*.so", dst="lib", src="lib")
       self.copy("*.so*", dst="lib", src="lib")
       self.copy("*.dylib", dst="lib", src="lib")
       self.copy("*.dylib", dst="tests", src="lib")
