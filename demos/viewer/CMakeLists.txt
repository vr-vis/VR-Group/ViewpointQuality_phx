#------------------------------------------------------------------------------
# Viewpoint Quality Lib
#
# Copyright (c) 2017-2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
#------------------------------------------------------------------------------
#                                 License
#
# Licensed under the 3-Clause BSD License (the "License");
# you may not use this file except in compliance with the License.
# See the file LICENSE for the full text.
# You may obtain a copy of the License at
#
#     https://opensource.org/licenses/BSD-3-Clause
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------------------------

file(GLOB VIEWER_SOURCES src/*.cpp)
file(GLOB VIEWER_HEADERS src/*.hpp)

add_executable(viewer
  ${VIEWER_SOURCES}
  ${VIEWER_HEADERS}
)
target_include_directories(viewer
  PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src
  vq
)
target_link_libraries(viewer
  vq
)
set_warning_levels_RWTH(viewer)
add_test_cpplint(NAME "viewer--cpplint"
  ${VIEWER_SOURCES}
  ${VIEWER_HEADERS}
)

add_test_cppcheck(NAME "viewer--cppcheck"
  ${VIEWER_SOURCES}
  ${VIEWER_HEADERS}
)
