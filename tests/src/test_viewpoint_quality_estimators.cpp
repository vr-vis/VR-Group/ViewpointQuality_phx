//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "catch/catch.hpp"

#include "vq/entropy.hpp"
#include "vq/visibility_histogram.hpp"

SCENARIO("We can compute viewpoint quality using (surface/object area) entropy",
         "[vq][vq::ViewpointQualityEstimator][vq::Entropy]") {
  GIVEN("A visibility histogram and a SurfaceAreaEntropy VQE") {
    vq::VisibilityHistogram hist;
    hist.Set(0, 0.2f);
    hist.Set(1, 0.3f);
    hist.Set(2, 0.5f);

    vq::Entropy vqe;

    WHEN("We compute the entropy") {
      float entropy_without_bg = vqe.GetViewpointQuality(&hist);
      vqe.SetIncludeBackground(true);
      float entropy_with_bg = vqe.GetViewpointQuality(&hist);
      vqe.SetExponential(true);
      float entropy_with_bg_exp = vqe.GetViewpointQuality(&hist);
      vqe.SetIncludeBackground(false);
      vqe.SetExponential(false);
      vqe.SetWeight(1, 2.f);
      vqe.SetWeight(2, 0.5f);
      float entropy_with_weight = vqe.GetViewpointQuality(&hist);
      THEN("It matches expectations") {
        REQUIRE(entropy_without_bg == Approx(1.021089678f));
        REQUIRE(entropy_with_bg == Approx(1.485475297f));
        REQUIRE(entropy_with_bg_exp == Approx(1.80009407241f));
        REQUIRE(entropy_with_weight == Approx(1.292179356f));
      }
    }
  }
}
