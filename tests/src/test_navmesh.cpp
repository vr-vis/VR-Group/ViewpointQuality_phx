//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <glm/glm.hpp>
#include <utility>
#include <vector>

#include "catch/catch.hpp"

#include "vq/navmesh.hpp"

SCENARIO("A navmesh can be queried for connectivity info",
         "[vq][vq::Navmesh]") {
  GIVEN("A navmesh with some vertices and triangles") {
    std::vector<glm::vec3> vertices = {
        {3.f, 0.f, 3.f},   {6.f, 0.f, 4.f},   {4.f, 0.f, 6.f},
        {9.f, 1.f, 7.f},   {10.f, -1.f, 3.f}, {13.5f, -2.f, 6.f},
        {12.f, 0.f, 11.f}, {6.f, -1.f, 10.f}, {10.f, 2.f, 13.f}};
    std::vector<glm::uvec3> triangles = {{0, 1, 2}, {1, 3, 2}, {1, 4, 3},
                                         {4, 5, 3}, {3, 5, 6}, {3, 6, 7},
                                         {3, 7, 2}, {7, 6, 8}};
    vq::Navmesh navmesh(vertices, triangles);

    THEN("We can query some vertex connectivity") {
      REQUIRE(navmesh.GetEdgeExists(1, 3));
      REQUIRE(navmesh.GetEdgeExists(3, 5));
      REQUIRE(navmesh.GetEdgeExists(8, 6));
      REQUIRE(navmesh.GetEdgeExists(2, 1));
      REQUIRE(navmesh.GetEdgeExists(6, 3));

      REQUIRE_FALSE(navmesh.GetEdgeExists(0, 3));
      REQUIRE_FALSE(navmesh.GetEdgeExists(3, 8));
      REQUIRE_FALSE(navmesh.GetEdgeExists(5, 7));
      REQUIRE_FALSE(navmesh.GetEdgeExists(2, 6));
      REQUIRE_FALSE(navmesh.GetEdgeExists(4, 2));
    }
    THEN("We can find out incident triangles") {
      REQUIRE(navmesh.GetIncidentTriangles(0) == std::vector<unsigned int>{0});
      REQUIRE(navmesh.GetIncidentTriangles(3) ==
              std::vector<unsigned int>({1, 2, 3, 4, 5, 6}));
      REQUIRE(navmesh.GetIncidentTriangles(4) ==
              std::vector<unsigned int>({2, 3}));
      REQUIRE(navmesh.GetIncidentTriangles(6) ==
              std::vector<unsigned int>({4, 5, 7}));

      REQUIRE(navmesh.GetIncidentTriangles(0, 1) ==
              std::vector<unsigned int>({0}));
      REQUIRE(navmesh.GetIncidentTriangles(3, 5) ==
              std::vector<unsigned int>({3, 4}));
      REQUIRE(navmesh.GetIncidentTriangles(8, 6) ==
              std::vector<unsigned int>({7}));
      REQUIRE(navmesh.GetIncidentTriangles(7, 3) ==
              std::vector<unsigned int>({5, 6}));
      REQUIRE(navmesh.GetIncidentTriangles(4, 3) ==
              std::vector<unsigned int>({2, 3}));
    }
    THEN("We can get a triangle's area") {
      REQUIRE(navmesh.GetTriangleArea(6) == Approx(9.97497f));
      REQUIRE(navmesh.GetTriangleArea(7) == Approx(9.89949f));
    }
    THEN("We can get the bounds") {
      REQUIRE(navmesh.GetBounds() ==
              std::make_pair(glm::vec3(3.f, -2.f, 3.f),
                             glm::vec3(13.5f, 2.f, 13.f)));
    }
  }
}

SCENARIO("We can determine the point on the navmesh below some point",
         "[vq][vq::Navmesh]") {
  GIVEN("A navmesh with some vertices and triangles, and two layers") {
    std::vector<glm::vec3> vertices = {
        {3.f, 0.f, 3.f},    {6.f, 0.f, 4.f},    {4.f, 0.f, 6.f},
        {9.f, 1.f, 7.f},    {10.f, -1.f, 3.f},  {13.5f, -2.f, 6.f},
        {12.f, 0.f, 11.f},  {6.f, -1.f, 10.f},  {10.f, 2.f, 13.f},
        {6.5f, -5.f, 2.5f}, {12.f, -6.f, 1.5f}, {12.f, -4.f, 5.5f},
        {8.f, -5.f, 8.f}};
    std::vector<glm::uvec3> triangles = {
        {0, 1, 2}, {1, 3, 2}, {1, 4, 3}, {4, 5, 3},   {3, 5, 6},
        {3, 6, 7}, {3, 7, 2}, {7, 6, 8}, {9, 10, 11}, {9, 11, 12}};
    vq::Navmesh navmesh(vertices, triangles);

    WHEN("We can check which triangle is below some point (if any)") {
      unsigned int triangle_idx;
      glm::vec3 projected_pos;

      THEN("No triangle if outside bounds") {
        REQUIRE_FALSE(navmesh.GetTriangleBelowPosition(
            {2.f, 5.f, 2.f}, triangle_idx, projected_pos));
        REQUIRE_FALSE(navmesh.GetTriangleBelowPosition(
            {5.f, 5.f, 15.f}, triangle_idx, projected_pos));
      }

      THEN("No triangle if inside bounds, but not above mesh") {
        REQUIRE_FALSE(navmesh.GetTriangleBelowPosition(
            {5.f, 5.f, 10.f}, triangle_idx, projected_pos));
        REQUIRE_FALSE(navmesh.GetTriangleBelowPosition(
            {13.f, 5.f, 3.f}, triangle_idx, projected_pos));
      }

      THEN("No triangle if below mesh") {
        REQUIRE_FALSE(navmesh.GetTriangleBelowPosition(
            {10.f, -10.f, 5.f}, triangle_idx, projected_pos));
      }

      THEN("Find triangle in one-layered case") {
        bool found = navmesh.GetTriangleBelowPosition(
            {4.f, 5.f, 4.f}, triangle_idx, projected_pos);
        REQUIRE(found);
        REQUIRE(triangle_idx == 0);
        REQUIRE(projected_pos == glm::vec3(4.f, 0.f, 4.f));

        found = navmesh.GetTriangleBelowPosition({4.135f, 10.f, 4.534f},
                                                 triangle_idx, projected_pos);
        REQUIRE(found);
        REQUIRE(triangle_idx == 0);
        REQUIRE(projected_pos == glm::vec3(4.135f, 0.f, 4.534f));

        found = navmesh.GetTriangleBelowPosition({9.f, 100.f, 11.f},
                                                 triangle_idx, projected_pos);
        REQUIRE(found);
        REQUIRE(triangle_idx == 7);
      }

      THEN("Find higher triangle in 2-layered case if we start above") {
        bool found = navmesh.GetTriangleBelowPosition(
            {9.f, 2.f, 5.f}, triangle_idx, projected_pos);
        REQUIRE(found);
        REQUIRE(triangle_idx == 2);
        REQUIRE(projected_pos.x == Approx(9.f));
        REQUIRE(projected_pos.z == Approx(5.f));
        REQUIRE(projected_pos.y > -1.f);
        REQUIRE(projected_pos.y < 1.f);
      }

      THEN("Find lower triangle in 2-layered case if we start between layers") {
        bool found = navmesh.GetTriangleBelowPosition(
            {9.f, -1.f, 5.f}, triangle_idx, projected_pos);
        REQUIRE(found);
        REQUIRE(triangle_idx == 9);
        REQUIRE(projected_pos.x == Approx(9.f));
        REQUIRE(projected_pos.z == Approx(5.f));
        REQUIRE(projected_pos.y > -6.f);
        REQUIRE(projected_pos.y < -4.f);
      }

      THEN(
          "Find triangle if below a vertex - if there are several triangles, "
          "it's always the one with the lowest index") {
        bool found = navmesh.GetTriangleBelowPosition(
            {3.f, 5.f, 3.f}, triangle_idx, projected_pos);
        REQUIRE(found);
        REQUIRE(triangle_idx == 0);
        REQUIRE(projected_pos == glm::vec3(3.f, 0.f, 3.f));

        found = navmesh.GetTriangleBelowPosition({9.f, 5.f, 7.f}, triangle_idx,
                                                 projected_pos);
        REQUIRE(found);
        REQUIRE(triangle_idx == 1);
        REQUIRE(projected_pos == glm::vec3(9.f, 1.f, 7.f));

        found = navmesh.GetTriangleBelowPosition({12.f, 5.f, 11.f},
                                                 triangle_idx, projected_pos);
        REQUIRE(found);
        REQUIRE(triangle_idx == 4);
        REQUIRE(projected_pos == glm::vec3(12.f, 0.f, 11.f));

        found = navmesh.GetTriangleBelowPosition({6.f, 5.f, 10.f}, triangle_idx,
                                                 projected_pos);
        REQUIRE(found);
        REQUIRE(triangle_idx == 5);
        REQUIRE(projected_pos == glm::vec3(6.f, -1.f, 10.f));
      }
    }

    WHEN("We can check which triangle is closest to some point") {
      bool found;
      unsigned int triangle_idx;
      glm::vec3 proj_pos_on_triangle;

      THEN("Normal case: the point is next to some triangle edge") {
        found = navmesh.GetClosestTriangleEdgeBelowPosition(
            {3.f, 2.f, 5.f}, triangle_idx, proj_pos_on_triangle);
        REQUIRE(found);
        REQUIRE(triangle_idx == 0);
        REQUIRE(proj_pos_on_triangle.x > 3.f);
        REQUIRE(proj_pos_on_triangle.x < 4.f);
        REQUIRE(proj_pos_on_triangle.z > 4.f);
        REQUIRE(proj_pos_on_triangle.z < 5.f);
        REQUIRE(proj_pos_on_triangle.y == Approx(0.f));

        found = navmesh.GetClosestTriangleEdgeBelowPosition(
            {12.f, 2.f, 12.f}, triangle_idx, proj_pos_on_triangle);
        REQUIRE(found);
        REQUIRE(triangle_idx == 7);
        REQUIRE(proj_pos_on_triangle.x > 11.f);
        REQUIRE(proj_pos_on_triangle.x < 12.f);
        REQUIRE(proj_pos_on_triangle.z > 11.f);
        REQUIRE(proj_pos_on_triangle.z < 12.f);
        REQUIRE(proj_pos_on_triangle.y > 0.f);
        REQUIRE(proj_pos_on_triangle.y < 1.f);
      }

      THEN("The closest position is on a vertex") {
        found = navmesh.GetClosestTriangleEdgeBelowPosition(
            {12.f, 2.1f, 15.f}, triangle_idx, proj_pos_on_triangle);
        REQUIRE(found);
        REQUIRE(triangle_idx == 7);
        REQUIRE(proj_pos_on_triangle == vertices[8]);
      }

      THEN("Below one layer of navmesh, next to the lower layer") {
        found = navmesh.GetClosestTriangleEdgeBelowPosition(
            {11.f, -2.f, 7.f}, triangle_idx, proj_pos_on_triangle);
        REQUIRE(triangle_idx == 9);
        REQUIRE(proj_pos_on_triangle.x > 10.f);
        REQUIRE(proj_pos_on_triangle.x < 11.f);
        REQUIRE(proj_pos_on_triangle.z > 6.f);
        REQUIRE(proj_pos_on_triangle.z < 7.f);
        REQUIRE(proj_pos_on_triangle.y > -5.f);
        REQUIRE(proj_pos_on_triangle.y < -4.f);
      }

      THEN("Not directly above any layer, but y is higher than both") {
        found = navmesh.GetClosestTriangleEdgeBelowPosition(
            {12.5f, 2.f, 4.5f}, triangle_idx, proj_pos_on_triangle);
        REQUIRE(triangle_idx == 3);
        REQUIRE(proj_pos_on_triangle.x > 12.f);
        REQUIRE(proj_pos_on_triangle.x < 13.f);
        REQUIRE(proj_pos_on_triangle.z > 4.f);
        REQUIRE(proj_pos_on_triangle.z < 5.f);
        REQUIRE(proj_pos_on_triangle.y > -2.f);
        REQUIRE(proj_pos_on_triangle.y < -1.f);
      }
    }

    WHEN(
        "We can get the closest vertex (= the closest vertex on the triangle "
        "below a position regarding barycentric coordinates)") {
      bool found;
      unsigned int vertex_idx;

      THEN("On the surface") {
        found = navmesh.GetClosestVertexOfClosestTriangleBelowPosition(
            {6.f, 2.f, 9.f}, vertex_idx);
        REQUIRE(found);
        REQUIRE(vertex_idx == 7);
      }
      THEN("Outside of the navmesh") {
        found = navmesh.GetClosestVertexOfClosestTriangleBelowPosition(
            {13.f, 0.f, 10.f}, vertex_idx);
        REQUIRE(found);
        REQUIRE(vertex_idx == 6);
      }
      THEN("Directly on some vertex") {
        found = navmesh.GetClosestVertexOfClosestTriangleBelowPosition(
            vertices[11], vertex_idx);
        REQUIRE(found);
        REQUIRE(vertex_idx == 11);
      }
    }
  }
}
