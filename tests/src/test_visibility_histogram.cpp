//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "catch/catch.hpp"

#include "vq/visibility_histogram.hpp"

SCENARIO("A visibility histogram can store and provide visibility data",
         "[vq][vq::VisibilityHistogram]") {
  GIVEN("An empty visibility histogram") {
    vq::VisibilityHistogram hist;

    THEN(
        "We can ask for visibility information for any entity, and it will "
        "always be 0") {
      REQUIRE(hist.Get(0) == 0.f);
      REQUIRE(hist.Get(5) == 0.f);
      REQUIRE(hist.Get(15) == 0.f);
    }

    THEN("The max entity is 0") { REQUIRE(hist.GetMaxEntity() == 0); }
    THEN("The sum is 0") { REQUIRE(hist.GetSum() == 0.f); }

    WHEN("We set visibility information for different entities") {
      hist.Set(5, 0.4f);
      hist.Set(1, 0.2f);
      hist.Set(100000, 0.1f);
      hist.Set(500, 0.15f);
      hist.Set(0, 0.05f);
      THEN("We can get it out again") {
        REQUIRE(hist.Get(5) == 0.4f);
        REQUIRE(hist.Get(1) == 0.2f);
        REQUIRE(hist.Get(100000) == 0.1f);
        REQUIRE(hist.Get(500) == 0.15f);
        REQUIRE(hist.Get(0) == 0.05f);
      }

      THEN("We can get the largest entity") {
        REQUIRE(hist.GetMaxEntity() == 100000);
      }
      THEN("We can get the sum") { REQUIRE(hist.GetSum() == Approx(0.9f)); }
      THEN("It is not normalized") { REQUIRE_FALSE(hist.GetNormalized()); }

      WHEN("We normalize the histogram") {
        hist.Normalize();
        THEN("Its sum is 1") { REQUIRE(hist.GetSum() == Approx(1.f)); }
        THEN("It is normalized") { REQUIRE(hist.GetNormalized()); }
      }

      THEN("We can find out the number of non-zero entries") {
        REQUIRE(hist.GetNumNonZeroEntries() == 5);
        hist.Set(1, 0.f);
        hist.Set(500, 0.f);
        REQUIRE(hist.GetNumNonZeroEntries() == 3);
      }
    }
  }
}

SCENARIO("We can mix visibility histograms", "[vq][vq::VisibilityHistogram]") {
  GIVEN("Two visibility histograms") {
    vq::VisibilityHistogram hist1, hist2;
    hist1.Set(0, 0.4f);
    hist1.Set(2, 0.5f);
    hist1.Set(5, 0.1f);
    hist2.Set(0, 0.3f);
    hist2.Set(1, 0.25f);
    hist2.Set(2, 0.25f);
    hist2.Set(3, 0.2f);

    WHEN("We mix them 25-75") {
      auto mixed_hist = hist1.GetMixed(&hist2, 0.75f);
      THEN("The mixed histogram is the weighted average of both") {
        REQUIRE(mixed_hist != nullptr);
        REQUIRE(mixed_hist->Get(0) == Approx(0.325f));
        REQUIRE(mixed_hist->Get(1) == Approx(0.1875f));
        REQUIRE(mixed_hist->Get(2) == Approx(0.3125f));
        REQUIRE(mixed_hist->Get(3) == Approx(0.15f));
        REQUIRE(mixed_hist->Get(4) == 0.f);
        REQUIRE(mixed_hist->Get(5) == Approx(0.025f));
      }
    }
    WHEN("We mix them 0-100") {
      auto mixed_hist = hist1.GetMixed(&hist2, 1.f);
      THEN("The mixed histogram is identical to the second one") {
        REQUIRE(hist2.Equals(mixed_hist.get()));
      }
    }
    WHEN("We mix them 100-0") {
      auto mixed_hist = hist1.GetMixed(&hist2, 0.f);
      THEN("The mixed histogram is identical to the first one") {
        REQUIRE(hist1.Equals(mixed_hist.get()));
      }
    }

    GIVEN("A third histogram") {
      vq::VisibilityHistogram hist3;
      hist3.Set(2, 0.1f);
      hist3.Set(5, 0.2f);
      hist3.Set(3, 0.4f);
      hist3.Set(10, 0.3f);

      WHEN("We mix all three of them 20-30-50") {
        auto mixed_hist = vq::VisibilityHistogram::GetMixed(
            &hist1, 0.2f, &hist2, 0.3f, &hist3);

        THEN("The mixed histogram matches expectations") {
          REQUIRE(mixed_hist != nullptr);
          REQUIRE(mixed_hist->Get(0) == Approx(0.17f));
          REQUIRE(mixed_hist->Get(1) == Approx(0.075f));
          REQUIRE(mixed_hist->Get(2) == Approx(0.225f));
          REQUIRE(mixed_hist->Get(3) == Approx(0.26f));
          REQUIRE(mixed_hist->Get(4) == 0.f);
          REQUIRE(mixed_hist->Get(5) == Approx(0.12f));
          REQUIRE(mixed_hist->Get(10) == Approx(0.15f));
        }
      }
      WHEN("We mix with some 0 as weight") {
        auto mixed12 = vq::VisibilityHistogram::GetMixed(&hist1, 0.3f, &hist2,
                                                         0.7f, &hist3);
        auto mixed13 = vq::VisibilityHistogram::GetMixed(&hist1, 0.3f, &hist2,
                                                         0.0f, &hist3);
        auto mixed23 = vq::VisibilityHistogram::GetMixed(&hist1, 0.0f, &hist2,
                                                         0.7f, &hist3);
        auto mixed2 = vq::VisibilityHistogram::GetMixed(&hist1, 0.0f, &hist2,
                                                        1.f, &hist3);
        THEN("The mixed histogram matches expectations") {
          REQUIRE(vq::VisibilityHistogram::GetMixed(&hist1, 0.3f, &hist2)
                      ->Equals(mixed12.get()));
          REQUIRE(vq::VisibilityHistogram::GetMixed(&hist1, 0.3f, &hist3)
                      ->Equals(mixed13.get()));
          REQUIRE(vq::VisibilityHistogram::GetMixed(&hist2, 0.7f, &hist3)
                      ->Equals(mixed23.get()));
          REQUIRE(hist2.Equals(mixed2.get()));
        }
      }

      WHEN("We compute the three-way intersection") {
        float intersect = vq::VisibilityHistogram::ComputeHistogramIntersection(
            &hist1, &hist2, &hist3);
        THEN("The resulting value matches expectations") {
          REQUIRE(intersect == Approx(0.1f));
        }
      }
    }

    WHEN("We compute the intersection") {
      float intersect = hist1.ComputeHistogramIntersection(&hist2);

      THEN("The resulting value matches expectations") {
        REQUIRE(intersect == Approx(0.55f));
      }
    }

    WHEN("We compute the Bhatthacharrya coefficient") {
      float bc = hist1.ComputeBhattacharyyaCoefficient(&hist2);
      THEN("The resulting value matches expectations") {
        REQUIRE(bc == Approx(0.69996f));
      }
    }
  }
}

SCENARIO("We can apply functions to all entries of a visibility histograms",
         "[vq][vq::VisibilityHistogram]") {
  GIVEN("A visibility histogram and a (1 or 2 parameter) lambda function") {
    vq::VisibilityHistogram hist1;
    hist1.Set(0, 0.4f);
    hist1.Set(2, 0.5f);
    hist1.Set(5, 0.1f);

    auto square = [](float f) { return f * f; };
    auto square_if_even = [](unsigned int idx, float f) {
      return (idx % 2 == 0 ? f * f : f);
    };

    WHEN(
        "We apply the 1 parameter function to all non-zero values of the "
        "histogram") {
      hist1.ApplyToAllNonZeroEntries(square);
      THEN("The resulting histogram mathces expectations") {
        REQUIRE(hist1.GetNumNonZeroEntries() == 3);
        REQUIRE(hist1.Get(0) == Approx(0.16f));
        REQUIRE(hist1.Get(2) == Approx(0.25f));
        REQUIRE(hist1.Get(5) == Approx(0.01f));
      }
    }
    WHEN(
        "We apply the 2 parameter function to all non-zero values of the "
        "histogram") {
      hist1.ApplyToAllNonZeroEntries(square_if_even);
      THEN("The resulting histogram mathces expectations") {
        REQUIRE(hist1.GetNumNonZeroEntries() == 3);
        REQUIRE(hist1.Get(0) == Approx(0.16f));
        REQUIRE(hist1.Get(2) == Approx(0.25f));
        REQUIRE(hist1.Get(5) == Approx(0.1f));
      }
    }
  }
}
