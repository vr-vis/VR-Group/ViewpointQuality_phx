//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <vector>

#include "catch/catch.hpp"

#include "vq/navmesh.hpp"
#include "vq/navmesh_data_provider.hpp"
#include "vq/static_grid.hpp"
#include "vq/visibility_histogram.hpp"

SCENARIO(
    "We can store viewpoint quality (float) data in a static grid"
    "[vq][vq::DataProvider][vq::StaticGrid]") {
  GIVEN("A static grid") {
    vq::StaticGridVQ grid(glm::vec3(-1.f, -1.f, -1.f), glm::vec3(1.f, 2.f, 3.f),
                          glm::uvec3(3, 2, 1));

    THEN("We can get its basic properties") {
      REQUIRE(grid.GetMin() == glm::vec3(-1.f, -1.f, -1.f));
      REQUIRE(grid.GetMax() == glm::vec3(1.f, 1.f, -1.f));
      REQUIRE(grid.GetSpacing() == glm::vec3(1.f, 2.f, 3.f));
      REQUIRE(grid.GetDimensions() == glm::uvec3(3, 2, 1));
    }

    WHEN("We set a few different data points") {
      grid.SetDataPoint(glm::vec3(0.f, -1.f, 0.f), 5.f);
      grid.SetDataPoint(glm::vec3(-1.f, -1.f, 0.f), 2.f);
      grid.SetDataPoint(glm::vec3(-1.f, 1.f, 0.f), 3.f);
      THEN("We can get them out again") {
        REQUIRE(grid.GetDataPoint(glm::vec3(0.f, -1.f, 0.f)) == 5.f);
        REQUIRE(grid.GetDataPoint(glm::vec3(-1.f, -1.f, 0.f)) == 2.f);
        REQUIRE(grid.GetDataPoint(glm::vec3(-1.f, 1.f, 0.f)) == 3.f);
      }

      WHEN("We set data points outside of the range") {
        grid.SetDataPoint(glm::vec3(1000.f, 1000.f, 1000.f), 10.f);
        grid.SetDataPoint(glm::vec3(0.f, 1000.f, 0.f), 5.f);
        grid.SetDataPoint(glm::vec3(-1000.f, -1000.f, -1000.f), -10.f);
        grid.SetDataPoint(glm::vec3(0.f, -1000.f, 0.f), -5.f);

        THEN("They are mapped to the borders of the grid") {
          REQUIRE(grid.GetDataPoint(glm::vec3(1.f, 1.f, -1.f)) == 10.f);
          REQUIRE(grid.GetDataPoint(glm::vec3(0.f, 1.f, -1.f)) == 5.f);
          REQUIRE(grid.GetDataPoint(glm::vec3(-1.f, -1.f, -1.f)) == -10.f);
          REQUIRE(grid.GetDataPoint(glm::vec3(0.f, -1.f, -1.f)) == -5.f);
        }
      }

      WHEN("We access positions between grid cells using interpolation") {
        THEN("They are interpolated linearly") {
          REQUIRE(grid.GetDataPoint(glm::vec3(-0.5f, -1.f, -1.f),
                                    vq::AccessStrategy::LINEAR_INTERPOLATION) ==
                  Approx(3.5f));
          REQUIRE(grid.GetDataPoint(glm::vec3(-0.5f, 0.f, -1.f),
                                    vq::AccessStrategy::LINEAR_INTERPOLATION) ==
                  Approx(2.5f));
          REQUIRE(grid.GetDataPoint(glm::vec3(-0.25f, -0.5f, -1.f),
                                    vq::AccessStrategy::LINEAR_INTERPOLATION) ==
                  Approx(3.375f));
        }
      }
    }
  }

  GIVEN("A static grid with z dimension > 1") {
    vq::StaticGridVQ grid(glm::vec3(0.f, 0.f, 0.f), glm::vec3(1.f, 1.f, 1.f),
                          glm::uvec3(2, 2, 2));
    WHEN("We set all data points") {
      grid.SetDataPoint(glm::vec3(0.f, 0.f, 0.f), 1.f);
      grid.SetDataPoint(glm::vec3(1.f, 0.f, 0.f), 2.f);
      grid.SetDataPoint(glm::vec3(0.f, 1.f, 0.f), 3.f);
      grid.SetDataPoint(glm::vec3(1.f, 1.f, 0.f), 4.f);
      grid.SetDataPoint(glm::vec3(0.f, 0.f, 1.f), 5.f);
      grid.SetDataPoint(glm::vec3(1.f, 0.f, 1.f), 6.f);
      grid.SetDataPoint(glm::vec3(0.f, 1.f, 1.f), 7.f);
      grid.SetDataPoint(glm::vec3(1.f, 1.f, 1.f), 8.f);

      WHEN(
          "We query a point that has to be interpolated across all "
          "dimensions") {
        float data_point_nn =
            grid.GetDataPoint(glm::vec3(0.25, 0.75f, 0.125f),
                              vq::AccessStrategy::NEAREST_NEIGHBOR);
        float data_point_interpolated =
            grid.GetDataPoint(glm::vec3(0.25, 0.75f, 0.125f),
                              vq::AccessStrategy::LINEAR_INTERPOLATION);
        THEN("It's what we expect") {
          REQUIRE(data_point_nn == 3.f);
          REQUIRE(data_point_interpolated == Approx(3.25f));
        }
      }
    }
  }
}

SCENARIO(
    "We can store visibility histograms in a static grid"
    "[vq][vq::DataProvider][vq::StaticGrid]") {
  GIVEN("A static grid") {
    vq::StaticGridVis grid(glm::vec3(-1.f, -1.f, -1.f),
                           glm::vec3(1.f, 2.f, 3.f), glm::uvec3(3, 2, 1));

    WHEN("We set a few different data points") {
      std::shared_ptr<vq::VisibilityHistogram> hist0 =
          std::make_shared<vq::VisibilityHistogram>();
      std::shared_ptr<vq::VisibilityHistogram> hist1 =
          std::make_shared<vq::VisibilityHistogram>();
      std::shared_ptr<vq::VisibilityHistogram> hist2 =
          std::make_shared<vq::VisibilityHistogram>();
      grid.SetDataPoint(glm::vec3(0.f, -1.f, 0.f), hist0);
      grid.SetDataPoint(glm::vec3(-1.f, -1.f, 0.f), hist1);
      grid.SetDataPoint(glm::vec3(-1.f, 1.f, 0.f), hist2);
      THEN("We can get them out again") {
        REQUIRE(grid.GetDataPoint(glm::vec3(0.f, -1.f, 0.f)) == hist0);
        REQUIRE(grid.GetDataPoint(glm::vec3(-1.f, -1.f, 0.f)) == hist1);
        REQUIRE(grid.GetDataPoint(glm::vec3(-1.f, 1.f, 0.f)) == hist2);
      }

      WHEN("We access positions between grid cells using interpolation") {
        THEN("They are interpolated linearly") {
          auto mix01 = hist0->GetMixed(hist1.get(), 0.5f);
          auto mix01_2 = mix01->GetMixed(hist2.get(), 0.5f);
          auto mix_between =
              hist0->GetMixed(hist1.get(), 0.25f)->GetMixed(hist2.get(), 0.25f);
          auto should_be_mix01 =
              grid.GetDataPoint(glm::vec3(-0.5f, -1.f, -1.f),
                                vq::AccessStrategy::LINEAR_INTERPOLATION);
          auto should_be_mix01_2 =
              grid.GetDataPoint(glm::vec3(-0.5f, 0.f, -1.f),
                                vq::AccessStrategy::LINEAR_INTERPOLATION);
          auto should_be_mix_between =
              grid.GetDataPoint(glm::vec3(-0.25f, -0.5f, -1.f),
                                vq::AccessStrategy::LINEAR_INTERPOLATION);
          REQUIRE(mix01->Equals(should_be_mix01.get()));
          REQUIRE(mix01_2->Equals(should_be_mix01_2.get()));
          REQUIRE(mix_between->Equals(should_be_mix_between.get()));
        }
      }
    }
  }
}

SCENARIO(
    "We can store viewpoint quality (float) data in a navmesh data provider"
    "[vq][vq::DataProvider][vq::NavmeshDataProvider]") {
  GIVEN("A navmesh and a navmesh data provider") {
    std::vector<glm::vec3> vertices = {{4.f, 0.f, 1.f}, {2.f, 0.f, 2.f},
                                       {6.f, 0.f, 2.f}, {2.f, 0.f, 5.f},
                                       {4.f, 0.f, 5.f}, {2.f, 0.f, 6.5f}};
    std::vector<glm::uvec3> triangles = {
        {0, 2, 1}, {1, 2, 4}, {1, 4, 3}, {3, 4, 5}};
    auto navmesh = std::make_shared<vq::Navmesh>(vertices, triangles);
    vq::NavMeshDataProviderVQ data_provider(navmesh);

    WHEN("We set data points") {
      data_provider.SetDataPoint(vertices[2], 2.f);
      data_provider.SetDataPoint(vertices[5], 5.f);
      data_provider.SetDataPoint(vertices[1], 1.f);
      data_provider.SetDataPoint(vertices[3], 3.f);
      THEN("We can get them out again") {
        REQUIRE(data_provider.GetDataPoint(vertices[2]) == 2.f);
        REQUIRE(data_provider.GetDataPoint(vertices[5]) == 5.f);
        REQUIRE(data_provider.GetDataPoint(vertices[1]) == 1.f);
        REQUIRE(data_provider.GetDataPoint(vertices[3]) == 3.f);
        REQUIRE(data_provider.GetDataDirect(2) == 2.f);
        REQUIRE(data_provider.GetDataDirect(5) == 5.f);
        REQUIRE(data_provider.GetDataDirect(1) == 1.f);
        REQUIRE(data_provider.GetDataDirect(3) == 3.f);
      }
    }

    WHEN("We set all data points") {
      for (std::size_t i = 0; i < vertices.size(); ++i) {
        data_provider.SetDataPoint(vertices[i], static_cast<float>(i));
      }

      THEN(
          "We can get interpolated values that match the values at vertex "
          "positions") {
        REQUIRE(data_provider.GetDataPoint(
                    vertices[3], vq::AccessStrategy::LINEAR_INTERPOLATION) ==
                3.f);
        REQUIRE(data_provider.GetDataPoint(
                    vertices[1], vq::AccessStrategy::LINEAR_INTERPOLATION) ==
                1.f);
        REQUIRE(data_provider.GetDataPoint(
                    vertices[0], vq::AccessStrategy::LINEAR_INTERPOLATION) ==
                0.f);
      }

      THEN("We can get interpolated values on the navmesh surface") {
        REQUIRE(data_provider.GetDataPoint(
                    {4.f, 2.f, 1.5f},
                    vq::AccessStrategy::LINEAR_INTERPOLATION) == Approx(0.75f));
        REQUIRE(
            data_provider.GetDataPoint(
                {4.f, 2.f, 2.5f}, vq::AccessStrategy::LINEAR_INTERPOLATION) ==
            Approx(1.91666667f));
        REQUIRE(
            data_provider.GetDataPoint(
                {3.5f, 2.f, 4.5f}, vq::AccessStrategy::LINEAR_INTERPOLATION) ==
            Approx(3.41666667f));
      }

      THEN("We can get interpolated values on a navmesh edge") {
        REQUIRE(data_provider.GetDataPoint(
                    {3.f, 2.f, 2.f},
                    vq::AccessStrategy::LINEAR_INTERPOLATION) == Approx(1.25f));
        REQUIRE(data_provider.GetDataPoint(
                    {3.f, 2.f, 3.5f},
                    vq::AccessStrategy::LINEAR_INTERPOLATION) == Approx(2.5f));
        REQUIRE(
            data_provider.GetDataPoint(
                {2.f, 2.f, 6.f}, vq::AccessStrategy::LINEAR_INTERPOLATION) ==
            Approx(4.33333333f));
      }

      THEN("We can get interpolated values for outside of the navmesh") {
        REQUIRE(
            data_provider.GetDataPoint(
                {1.f, 2.f, 4.f}, vq::AccessStrategy::LINEAR_INTERPOLATION) ==
            Approx(2.33333f));
        REQUIRE(data_provider.GetDataPoint(
                    {6.5f, 2.f, 4.5f},
                    vq::AccessStrategy::LINEAR_INTERPOLATION) == Approx(3.f));
        REQUIRE(data_provider.GetDataPoint(
                    {0.f, 2.f, 9.f},
                    vq::AccessStrategy::LINEAR_INTERPOLATION) == Approx(5.f));
      }
    }
  }
}

void SetHistogramByFunction(std::shared_ptr<vq::VisibilityHistogram> hist,
                            const std::vector<unsigned int>& bins,
                            const std::function<float(unsigned int)>& func) {
  for (unsigned int bin : bins) {
    hist->Set(bin, func(bin));
  }
  hist->Normalize();
}

SCENARIO(
    "We can store visibility (histogram) data in a navmesh data provider"
    "[vq][vq::DataProvider][vq::NavmeshDataProvider]") {
  GIVEN("A navmesh and a navmesh data provider, and some histograms.") {
    std::vector<glm::vec3> vertices = {
        {4.f, 0.f, 1.f}, {2.f, 0.f, 2.f},  {6.f, 0.f, 2.f}, {2.f, 0.f, 5.f},
        {4.f, 0.f, 5.f}, {2.f, 0.f, 6.5f}, {1.f, 0.f, 6.f}};
    std::vector<glm::uvec3> triangles = {
        {0, 2, 1}, {1, 2, 4}, {1, 4, 3}, {3, 4, 5}, {3, 5, 6}};
    auto navmesh = std::make_shared<vq::Navmesh>(vertices, triangles);
    vq::NavMeshDataProviderVis data_provider(navmesh);

    auto hist0 = std::make_shared<vq::VisibilityHistogram>();
    auto hist1 = std::make_shared<vq::VisibilityHistogram>();
    auto hist2 = std::make_shared<vq::VisibilityHistogram>();
    auto hist4 = std::make_shared<vq::VisibilityHistogram>();
    SetHistogramByFunction(hist0, {0, 1, 2, 3, 4, 5}, [](unsigned int i) {
      return static_cast<float>(i);
    });
    SetHistogramByFunction(hist1, {0, 1, 2, 3, 4, 5}, [](unsigned int i) {
      return static_cast<float>(i * i);
    });
    SetHistogramByFunction(hist2, {0, 1, 2, 3, 4, 5}, [](unsigned int i) {
      return std::sqrt(static_cast<float>(i));
    });
    SetHistogramByFunction(hist4, {0, 1, 2, 3, 4, 5},
                           [](unsigned int) { return 1.f; });

    WHEN("We set data points") {
      data_provider.SetDataPoint(vertices[1], hist1);
      data_provider.SetDataPoint(vertices[2], hist2);
      THEN("We can get them out again") {
        REQUIRE(hist1->Equals(data_provider.GetDataPoint(vertices[1])));
        REQUIRE(hist2->Equals(data_provider.GetDataPoint(vertices[2])));
        REQUIRE(data_provider.GetDataDirect(1) == hist1);
        REQUIRE(data_provider.GetDataDirect(2) == hist2);
      }
    }

    WHEN("We set all data points except for vertices 3, 5, 6") {
      data_provider.SetDataPoint(vertices[0], hist0);
      data_provider.SetDataPoint(vertices[1], hist1);
      data_provider.SetDataPoint(vertices[2], hist2);
      data_provider.SetDataPoint(vertices[4], hist4);

      THEN(
          "We can get interpolated values that match the values at vertex "
          "positions") {
        REQUIRE(hist4->Equals(data_provider.GetDataPoint(
            vertices[4], vq::AccessStrategy::LINEAR_INTERPOLATION)));
        REQUIRE(hist1->Equals(data_provider.GetDataPoint(
            vertices[1], vq::AccessStrategy::LINEAR_INTERPOLATION)));
        REQUIRE(hist0->Equals(data_provider.GetDataPoint(
            vertices[0], vq::AccessStrategy::LINEAR_INTERPOLATION)));
      }

      THEN("We can get interpolated values on the navmesh surface") {
        auto hist_check = data_provider.GetDataPoint(
            {4.f, 2.f, 1.5f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        auto should_be =
            vq::VisibilityHistogram::GetMixed(hist0, 0.5f, hist1, 0.25f, hist2);
        REQUIRE(vq::VisibilityHistogram::ComputeHistogramIntersection(
                    should_be, hist_check) == Approx(1.f));
        hist_check = data_provider.GetDataPoint(
            {4.f, 2.f, 2.5f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        should_be = vq::VisibilityHistogram::GetMixed(hist4, 1.f / 6.f, hist1,
                                                      2.5f / 6.f, hist2);
        REQUIRE(vq::VisibilityHistogram::ComputeHistogramIntersection(
                    should_be, hist_check) == Approx(1.f));
      }
      THEN(
          "If one histogram is not set, it is ignored and only the remaining "
          "two are interpolated") {
        auto hist_check = data_provider.GetDataPoint(
            {3.5f, 2.f, 4.5f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        auto should_be = vq::VisibilityHistogram::GetMixedIgnoreNull(
            hist1, 1.f / 6.f, hist4, 0.75f, nullptr);
        REQUIRE(vq::VisibilityHistogram::ComputeHistogramIntersection(
                    should_be, hist_check) == Approx(1.f));
      }
      THEN(
          "If two histograms are not set, the value is always the one of the "
          "only set vertex") {
        auto hist_check = data_provider.GetDataPoint(
            {3.f, 2.f, 5.5f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        REQUIRE(hist_check->Equals(hist4));
      }
      THEN("If no histogram is set, the result should be null") {
        REQUIRE(data_provider.GetDataPoint(
                    {1.5f, 2.f, 6.f},
                    vq::AccessStrategy::LINEAR_INTERPOLATION) == nullptr);
      }

      THEN("We can get interpolated histograms on a navmesh edge") {
        auto check = data_provider.GetDataPoint(
            {3.f, 2.f, 2.f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        auto should_be = vq::VisibilityHistogram::GetMixed(hist1, 0.75f, hist2);
        REQUIRE(vq::VisibilityHistogram::ComputeHistogramIntersection(
                    check, should_be) == Approx(1.f));

        check = data_provider.GetDataPoint(
            {3.f, 2.f, 3.5f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        should_be = vq::VisibilityHistogram::GetMixed(hist1, 0.5f, hist4);
        REQUIRE(vq::VisibilityHistogram::ComputeHistogramIntersection(
                    check, should_be) == Approx(1.f));
      }

      THEN("We can get interpolated histograms for outside of the navmesh") {
        auto check = data_provider.GetDataPoint(
            {1.f, 2.f, 4.f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        REQUIRE(vq::VisibilityHistogram::ComputeHistogramIntersection(
                    check, hist1) == Approx(1.f));
        auto hist3 = hist2->Copy();
        data_provider.SetDataPoint(vertices[3], hist3);
        check = data_provider.GetDataPoint(
            {1.f, 2.f, 4.f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        auto should_be1 =
            vq::VisibilityHistogram::GetMixed(hist1, 0.3333333f, hist3);
        REQUIRE(vq::VisibilityHistogram::ComputeHistogramIntersection(
                    check, should_be1) == Approx(1.f));

        check = data_provider.GetDataPoint(
            {6.5f, 2.f, 4.5f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        auto should_be2 = vq::VisibilityHistogram::GetMixed(hist2, 0.5f, hist4);
        REQUIRE(vq::VisibilityHistogram::ComputeHistogramIntersection(
                    check, should_be2) == Approx(1.f));

        check = data_provider.GetDataPoint(
            {12.f, 2.f, 2.f}, vq::AccessStrategy::LINEAR_INTERPOLATION);
        auto should_be3 = hist2;
        REQUIRE(vq::VisibilityHistogram::ComputeHistogramIntersection(
                    check, should_be3) == Approx(1.f));
      }
    }
  }
}
