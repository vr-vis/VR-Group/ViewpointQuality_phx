#------------------------------------------------------------------------------
# Viewpoint Quality Lib
#
# Copyright (c) 2017-2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
#------------------------------------------------------------------------------
#                                 License
#
# Licensed under the 3-Clause BSD License (the "License");
# you may not use this file except in compliance with the License.
# See the file LICENSE for the full text.
# You may obtain a copy of the License at
#
#     https://opensource.org/licenses/BSD-3-Clause
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------------------------

#see at the bottom how to add mocked tests

# configure reference image directory include string
set(_REFIMAGE_ROOT_PATH "${CMAKE_CURRENT_SOURCE_DIR}/reference_images/")
set(SOURCE_REFIMAGE_INCLUDE_FILE "${CMAKE_CURRENT_SOURCE_DIR}/test_utilities/reference_image_path.hpp.template")
set(TARGET_REFIMAGE_INCLUDE_FILE "${CMAKE_CURRENT_BINARY_DIR}/test_utilities/reference_image_path.hpp")
configure_file(${SOURCE_REFIMAGE_INCLUDE_FILE} ${TARGET_REFIMAGE_INCLUDE_FILE} @ONLY)

file(GLOB VQ_TEST_SOURCES src/*.cpp)
file(GLOB VQ_TEST_HEADERS src/*.hpp)
file(GLOB VQ_TEST_UTILITIES_TEST_SOURCES test_utilities/tests/src/*.cpp)
file(GLOB VQ_TEST_UTILITIES_HEADERS test_utilities/*.hpp)
file(GLOB VQ_TEST_UTILITIES_SOURCES test_utilities/*.cpp)

#get the include directories
get_target_property(glew_include_directories
  ${CONAN_OR_CMAKE_glew} INTERFACE_INCLUDE_DIRECTORIES)
get_target_property(vq_include_directories 
  vq INCLUDE_DIRECTORIES)
get_target_property(sdl2_include_directories
  ${CONAN_OR_CMAKE_SDL2} INTERFACE_INCLUDE_DIRECTORIES)
get_target_property(gl_include_directories
  ${CONAN_OR_CMAKE_gl} INTERFACE_INCLUDE_DIRECTORIES)


# generate test_utilities
add_library(test_utilities STATIC
  ${VQ_TEST_UTILITIES_SOURCES}
  ${VQ_TEST_UTILITIES_HEADERS}
)
set_target_properties(test_utilities PROPERTIES LINKER_LANGUAGE CXX)
set_property(TARGET test_utilities PROPERTY FOLDER "Tests")
target_include_directories(test_utilities
  PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} 
  PUBLIC ${CMAKE_CURRENT_BINARY_DIR})
target_link_libraries(test_utilities
  ${CONAN_OR_CMAKE_gl}
  ${CONAN_OR_CMAKE_catch}
  vq)
set_warning_levels_rwth(test_utilities)


#get the gl link library
if(TARGET CONAN_LIB::gl_gl)
  get_target_property(gl_link_library CONAN_LIB::gl_gl LOCATION)
elseif(TARGET CONAN_LIB::gl_glrelease)
  get_target_property(gl_imported_location_rel CONAN_LIB::gl_glrelease LOCATION)
  get_target_property(gl_imported_location_deb CONAN_LIB::gl_gldebug LOCATION)
  set(gl_link_library "$<$<CONFIG:Release>:${gl_imported_location_rel}>$<$<CONFIG:Debug>:${gl_imported_location_deb}>")
else()
  message(FATAL_ERROR "You are using a conan version older than 0.29, please update e.g. by \"pip install conan --upgrade\"")
endif()

set(IS_BUILD_SERVER FALSE CACHE BOOL "Is this the build server? So we, e.g., simulate user input for tests requiring it.")



# these have to go before the mocked tests.
# Otherwise, the mocked ones are not in VQ_TEST_SOURCES anymore
add_test_cppcheck(NAME "vq-tests--cppcheck"
  ${VQ_TEST_SOURCES}
  ${VQ_TEST_HEADERS}
  ${VQ_TEST_UTILITIES_TEST_SOURCES}
  ${VQ_TEST_UTILITIES_HEADERS}
  ${VQ_TEST_UTILITIES_SOURCES}
  )
  
add_test_cpplint(NAME "vq-tests--cpplint"
  ${VQ_TEST_SOURCES}
  ${VQ_TEST_HEADERS}
  ${VQ_TEST_UTILITIES_TEST_SOURCES}
  ${VQ_TEST_UTILITIES_HEADERS}
  ${VQ_TEST_UTILITIES_SOURCES}
)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------



add_test_catch(NAME "vq-tests"
  SOURCES ${VQ_TEST_SOURCES} ${VQ_TEST_UTILITIES_TEST_SOURCES}
  HEADERS ${VQ_TEST_HEADERS}
  CATCH_MAIN src/tests.cpp
  INCLUDE_DIRECTORIES ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR} vq
  LINK_LIBRARIES vq test_utilities
  PATH_TO_ADD ${PROJECT_BINARY_DIR}/library
)


