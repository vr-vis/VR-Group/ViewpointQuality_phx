//------------------------------------------------------------------------------
// Viewpoint Quality
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "viewpoint_quality_estimator.hpp"

namespace vq {

void ViewpointQualityEstimator::SetWeight(unsigned int entity_idx,
                                          float weight) {
  if (weights_.size() <= entity_idx) weights_.resize(entity_idx + 1, 1.f);
  weights_[entity_idx] = weight;
}

float ViewpointQualityEstimator::GetWeight(unsigned int entity_idx) const {
  if (weights_.size() <= entity_idx) return 1.f;
  return weights_[entity_idx];
}

void ViewpointQualityEstimator::ResetWeights() { weights_.clear(); }

}  // namespace vq
