//------------------------------------------------------------------------------
// Viewpoint Quality
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "navmesh.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/gtx/norm.hpp>

#include <algorithm>
#include <cassert>
#include <limits>
#include <utility>
#include <vector>

namespace vq {

Navmesh::Navmesh(const std::vector<glm::vec3>& vertices,
                 const std::vector<glm::uvec3>& triangles)
    : vertices_(vertices), triangles_(triangles) {
  ComputeBounds();
}

bool Navmesh::GetEdgeExists(unsigned int vertex_idx1,
                            unsigned int vertex_idx2) {
  if (adjacency_structure_dirty_) BuildAdjacencyStructure();

  auto it = std::lower_bound(edges_[vertex_idx1].begin(),
                             edges_[vertex_idx1].end(), vertex_idx2);
  if (it != edges_[vertex_idx1].end() && *it == vertex_idx2) return true;
  return false;
}

const std::vector<unsigned int>& Navmesh::GetNeighbors(
    unsigned int vertex_idx) {
  if (adjacency_structure_dirty_) BuildAdjacencyStructure();
  return edges_[vertex_idx];
}

const std::vector<unsigned int>& Navmesh::GetIncidentTriangles(
    unsigned int vertex_idx) {
  if (adjacency_structure_dirty_) BuildAdjacencyStructure();
  return incident_triangles_[vertex_idx];
}

std::vector<unsigned int> Navmesh::GetIncidentTriangles(
    unsigned int vertex_idx1, unsigned int vertex_idx2) {
  if (adjacency_structure_dirty_) BuildAdjacencyStructure();
  // find triangles incident to both vertices
  std::vector<unsigned int> incident_to_edge;
  for (unsigned int triangle_idx : incident_triangles_[vertex_idx1]) {
    if (std::binary_search(incident_triangles_[vertex_idx2].begin(),
                           incident_triangles_[vertex_idx2].end(),
                           triangle_idx))
      incident_to_edge.push_back(triangle_idx);
  }
  return incident_to_edge;
}

float Navmesh::GetTriangleArea(unsigned int triangle_idx) const {
  glm::vec3 bma = vertices_[triangles_[triangle_idx].y] -
                  vertices_[triangles_[triangle_idx].x];
  glm::vec3 cma = vertices_[triangles_[triangle_idx].z] -
                  vertices_[triangles_[triangle_idx].x];
  return 0.5f * glm::length(glm::cross(bma, cma));
}

bool Navmesh::GetTriangleBelowPosition(const glm::vec3& pos,
                                       unsigned int& triangle_idx,
                                       glm::vec3& projected_pos_on_triangle) {
  if (acceleration_structure_dirty_) BuildAccelerationStructure();

  // if outside bounding box: no hit
  if (pos[0] < bounds_.first[0] || pos[0] > bounds_.second[0] ||
      pos[2] < bounds_.first[2] || pos[2] > bounds_.second[2])
    return false;

  std::size_t grid_index = GetAccGridIndex(GetAccGridCoords(pos));

  // determine possible candidates (more than one if there are several triangles
  // above each other)
  // store the coordinates of the projection of pos onto
  // the triangle and the idx of the triangle
  std::vector<std::pair<unsigned int, glm::vec3>> candidates;

  // iterate through all triangles in this acc grid cell and compute
  // intersection tests; store candidate triangles to choose from
  for (unsigned int triangle_candidate_idx :
       acc_grid_triangles_by_cell_[grid_index]) {
    // intersection test down onto triangle
    glm::vec3 bary_pos;
    bool intersect = glm::intersectRayTriangle(
        pos, glm::vec3(0.f, -1.f, 0.f),
        vertices_[triangles_[triangle_candidate_idx].x],
        vertices_[triangles_[triangle_candidate_idx].y],
        vertices_[triangles_[triangle_candidate_idx].z], bary_pos);
    // note: bary_pos.x and .y are the barycentric coordinates for the
    // SECOND and THIRD vertex
    // note: bary_pos.z is not written to
    bary_pos.z = 1.f - bary_pos.x - bary_pos.y;

    glm::vec3 contact_point =
        bary_pos.z * vertices_[triangles_[triangle_candidate_idx].x] +
        bary_pos.x * vertices_[triangles_[triangle_candidate_idx].y] +
        bary_pos.y * vertices_[triangles_[triangle_candidate_idx].z];

    if (intersect) {
      candidates.push_back(
          std::make_pair(triangle_candidate_idx, contact_point));
    }
  }

  // candidates is typically of size 0 or 1 here, so catch these special
  // cases
  if (candidates.empty()) return false;
  if (candidates.size() == 1) {
    triangle_idx = candidates.front().first;
    projected_pos_on_triangle = candidates.front().second;
    return true;
  }

  // otherwise: select the one with the highest y coordinate
  // (which is automatically below pos because of the downwards intersection
  // test)
  float ymax = candidates[0].second[1];
  std::size_t max_index = 0;
  for (std::size_t i = 1; i < candidates.size(); ++i) {
    if (candidates[i].second[1] > ymax) {
      ymax = candidates[i].second[1];
      max_index = i;
    }
  }

  triangle_idx = candidates[max_index].first;
  projected_pos_on_triangle = candidates[max_index].second;
  return true;
}

bool Navmesh::GetClosestTriangleEdgeBelowPosition(
    const glm::vec3& pos, unsigned int& triangle_idx,
    glm::vec3& projected_pos_on_triangle, float max_distance /*= 25.f*/) {
  if (acceleration_structure_dirty_) BuildAccelerationStructure();

  // search from center to outside
  // determine all possible grid cells in each layer roughly within max_distance
  // as soon as we found something, we can stop as soon as we get to the next
  // max-distance level

  bool found = false;

  // map distance to grid coordinates d -> (x, z)
  std::vector<std::pair<unsigned int, std::pair<unsigned int, unsigned int>>>
      candidate_grid_cells;

  // get center
  glm::uvec2 center_grid_coords = GetAccGridCoords(pos);

  // some helper functions
  auto GetIsValidCell = [this](unsigned int x, unsigned int z) -> bool {
    return (x < acc_grid_dims_[0]) && (z < acc_grid_dims_[1]);
  };
  auto GetDistance = [](unsigned int x0, unsigned int z0, unsigned int x1,
                        unsigned int z1) -> unsigned int {
    unsigned int uiZDist = (z1 >= z0) ? (z1 - z0) : (z0 - z1);
    unsigned int uiXDist = (x1 >= x0) ? (x1 - x0) : (x0 - x1);
    return std::max(uiXDist, uiZDist);
  };
  auto AddCell = [&candidate_grid_cells, &GetDistance, &GetIsValidCell,
                  &center_grid_coords](unsigned int x, unsigned int z) {
    if (GetIsValidCell(x, z)) {
      candidate_grid_cells.push_back(std::make_pair(
          GetDistance(x, z, center_grid_coords[0], center_grid_coords[1]),
          std::make_pair(x, z)));
    }
  };

  unsigned int max_grid_distance =
      (unsigned int)std::ceil(max_distance / acc_grid_sidelength_);

  for (unsigned int current_grid_dist = 0;
       current_grid_dist <= max_grid_distance; ++current_grid_dist) {
    // add all cells with current_grid_dist
    if (current_grid_dist == 0) {
      // 0 is only center
      AddCell(center_grid_coords[0], center_grid_coords[1]);
    } else {
      // add +z and -z side (including corners)
      unsigned int xmin = center_grid_coords[0] - current_grid_dist;
      if (xmin > center_grid_coords[0])  // UNDERflow (unsigned)
        xmin = 0;
      unsigned int xmax = std::min(
          acc_grid_dims_[0], center_grid_coords[0] + current_grid_dist + 1);

      for (unsigned int x = xmin; x < xmax; ++x) {
        AddCell(x, center_grid_coords[1] + current_grid_dist);
        AddCell(x,
                center_grid_coords[1] -
                    current_grid_dist);  // z may underflow, but is invalid then
      }

      // add +x and -x side (excluding corners)
      unsigned int zmin = center_grid_coords[1] - current_grid_dist + 1;
      if (zmin > center_grid_coords[1])  // UNDERflow (unsigned)
        zmin = 0;
      unsigned int zmax = std::min(acc_grid_dims_[1],
                                   center_grid_coords[1] + current_grid_dist);

      for (unsigned int z = zmin; z < zmax; ++z) {
        AddCell(center_grid_coords[0] + current_grid_dist, z);
        AddCell(center_grid_coords[0] - current_grid_dist,
                z);  // x may underflow, but is invalid then
      }
    }

    // save all possible candidates
    struct Candidate {
      unsigned int triangle_idx;
      float dist_squared;
      glm::vec3 proj_pos;
    };

    std::vector<Candidate> candidates;

    // okay, now check all our candidates
    for (const auto& cand : candidate_grid_cells) {
      assert(cand.first == current_grid_dist || cand.first == 0);

      // find grid cell index
      std::size_t grid_index =
          GetAccGridIndex(cand.second.first, cand.second.second);
      // for all triangles in the cell...
      for (unsigned int triangle_idx :
           acc_grid_triangles_by_cell_[grid_index]) {
        // project onto the three edges of the triangle (clamp to actual edge,
        // not the continuation of the edge as line), and choose the closest of
        // the three points as candidate for the closest point

        const glm::vec3& a = vertices_[triangles_[triangle_idx].x];
        const glm::vec3& b = vertices_[triangles_[triangle_idx].y];
        const glm::vec3& c = vertices_[triangles_[triangle_idx].z];

        glm::vec3 ab = (b - a);
        float ab_length = glm::length(ab);
        ab /= ab_length;

        glm::vec3 ac = (c - a);
        float ac_length = glm::length(ac);
        ac /= ac_length;

        glm::vec3 bc = (c - b);
        float bc_length = glm::length(bc);
        bc /= bc_length;

        // project point vertically down onto the (infinite) triangle plane
        glm::vec3 triangle_normal = glm::normalize(glm::cross(ac, ab));
        float intersection_distance;
        bool intersect =
            glm::intersectRayPlane(pos, glm::vec3(0.f, -1.f, 0.f), a,
                                   triangle_normal, intersection_distance);
        glm::vec3 proj_point =
            pos + glm::vec3(0.f, -1.f, 0.f) * intersection_distance;

        if (intersect) {
          // A-B
          glm::vec3 ap = (proj_point - a);
          float segment = glm::dot(ap, ab);
          segment = glm::clamp(segment, 0.f, ab_length);
          glm::vec3 closest_candidate_ab = a + segment * ab;

          // A-C
          segment = glm::dot(ap, ac);
          segment = glm::clamp(segment, 0.f, ac_length);
          glm::vec3 closest_candidate_ac = a + segment * ac;

          // B-C
          glm::vec3 bp = (proj_point - b);
          segment = glm::dot(bp, bc);
          segment = glm::clamp(segment, 0.f, bc_length);
          glm::vec3 closest_candidate_bc = b + segment * bc;

          float dist_sq_cand_ab = glm::length2(closest_candidate_ab - pos);
          float dist_sq_cand_ac = glm::length2(closest_candidate_ac - pos);
          float dist_sq_cand_bc = glm::length2(closest_candidate_bc - pos);

          if (closest_candidate_ab.y < pos.y) {
            candidates.push_back(
                {triangle_idx, dist_sq_cand_ab, closest_candidate_ab});
          }
          if (closest_candidate_ac.y < pos.y) {
            candidates.push_back(
                {triangle_idx, dist_sq_cand_ac, closest_candidate_ac});
          }
          if (closest_candidate_bc.y < pos.y) {
            candidates.push_back(
                {triangle_idx, dist_sq_cand_bc, closest_candidate_bc});
          }
        }
      }
    }

    // have we found any candidates? if so, we don't need to look any further.
    if (!candidates.empty()) {
      // get the lowest-distance candidate
      float lowest_dist = std::numeric_limits<float>::infinity();
      Candidate& best_cand = candidates[0];

      for (const Candidate& cand : candidates) {
        if (cand.dist_squared < lowest_dist && cand.proj_pos[1] < pos[1]) {
          best_cand = cand;
          lowest_dist = cand.dist_squared;
        }
      }

      // copy result data
      triangle_idx = best_cand.triangle_idx;
      projected_pos_on_triangle = best_cand.proj_pos;

      // found something!
      found = true;

      // stop, because all that comes has to be farther away
      break;
    }

    // clear for next round!
    candidate_grid_cells.clear();
  }

  return found;
}

bool Navmesh::GetClosestVertexOfClosestTriangleBelowPosition(
    const glm::vec3& pos, unsigned int& vertex_idx,
    /* NOLINT */ float max_check_distance /*= 25.f*/) {
  // check directly below
  unsigned int triangle_idx;
  glm::vec3 proj_point_on_triangle;
  if (!GetTriangleBelowPosition(pos, triangle_idx, proj_point_on_triangle)) {
    if (!GetClosestTriangleEdgeBelowPosition(
            pos, triangle_idx, proj_point_on_triangle, max_check_distance)) {
      // not found
      return false;
    } else {
      glm::vec3 barycentric;
      glm::intersectLineTriangle(
          proj_point_on_triangle, glm::vec3(0.f, 1.f, 0.f),
          vertices_[triangles_[triangle_idx].x],
          vertices_[triangles_[triangle_idx].y],
          vertices_[triangles_[triangle_idx].z], barycentric);
      barycentric.x = 1.f - barycentric.y - barycentric.z;
      // we're either on an edge or on a vertex
      // on an edge, we want to return the closest of the two edge vertices
      // (even if the third vertex is closer)
      // on the vertex, we just want this vertex
      // => just return the one with the largest barycentric coordinate
      if (barycentric.x >= barycentric.y && barycentric.x >= barycentric.z) {
        vertex_idx = triangles_[triangle_idx].x;
        return true;
      } else if (barycentric.y >= barycentric.x &&
                 barycentric.y >= barycentric.z) {
        vertex_idx = triangles_[triangle_idx].y;
        return true;
      } else {
        vertex_idx = triangles_[triangle_idx].z;
        return true;
      }
    }
  } else {
    // find closest vertex of that triangle
    float sqdist_to_x = glm::length2(vertices_[triangles_[triangle_idx].x] -
                                     proj_point_on_triangle);
    float sqdist_to_y = glm::length2(vertices_[triangles_[triangle_idx].y] -
                                     proj_point_on_triangle);
    float sqdist_to_z = glm::length2(vertices_[triangles_[triangle_idx].z] -
                                     proj_point_on_triangle);

    if (sqdist_to_x <= sqdist_to_y && sqdist_to_x <= sqdist_to_z) {
      vertex_idx = triangles_[triangle_idx].x;
      return true;
    } else if (sqdist_to_y <= sqdist_to_x && sqdist_to_y <= sqdist_to_z) {
      vertex_idx = triangles_[triangle_idx].y;
      return true;
    } else {
      vertex_idx = triangles_[triangle_idx].z;
      return true;
    }
  }
}

void Navmesh::BuildAdjacencyStructure() {
  // edges - doubly connected
  edges_.clear();
  edges_.resize(vertices_.size());
  // triangles incident to vertex
  incident_triangles_.clear();
  incident_triangles_.resize(vertices_.size());

  unsigned int triangle_idx = 0;
  for (const auto& tri : triangles_) {
    edges_[tri.x].push_back(tri.y);
    edges_[tri.x].push_back(tri.z);
    edges_[tri.y].push_back(tri.x);
    edges_[tri.y].push_back(tri.z);
    edges_[tri.z].push_back(tri.x);
    edges_[tri.z].push_back(tri.y);

    incident_triangles_[tri.x].push_back(triangle_idx);
    incident_triangles_[tri.y].push_back(triangle_idx);
    incident_triangles_[tri.z].push_back(triangle_idx);

    ++triangle_idx;
  }
  // sort adjacency lists for quicker access
  for (auto& adj_list : edges_) {
    std::sort(adj_list.begin(), adj_list.end());
  }
  // sort incidence lists
  for (auto& inc_list : incident_triangles_) {
    std::sort(inc_list.begin(), inc_list.end());
  }

  adjacency_structure_dirty_ = false;
}

void Navmesh::ComputeBounds() {
  if (vertices_.empty()) return;
  bounds_.first = vertices_[0];
  bounds_.second = vertices_[0];
  for (const auto& vertex : vertices_) {
    bounds_.first.x = std::min(bounds_.first.x, vertex.x);
    bounds_.first.y = std::min(bounds_.first.y, vertex.y);
    bounds_.first.z = std::min(bounds_.first.z, vertex.z);
    bounds_.second.x = std::max(bounds_.second.x, vertex.x);
    bounds_.second.y = std::max(bounds_.second.y, vertex.y);
    bounds_.second.z = std::max(bounds_.second.z, vertex.z);
  }
}

std::vector<std::size_t> Navmesh::GetAccGridIndicesAlongEdge(
    const glm::vec3& start, const glm::vec3& end) {
  std::vector<std::size_t> indices;

  // first: convert positions into grid coordinates
  float x0 =
      std::max(0.f, (start[0] - bounds_.first[0]) / acc_grid_sidelength_);
  float z0 =
      std::max(0.f, (start[2] - bounds_.first[2]) / acc_grid_sidelength_);
  float x1 = std::max(0.f, (end[0] - bounds_.first[0]) / acc_grid_sidelength_);
  float z1 = std::max(0.f, (end[2] - bounds_.first[2]) / acc_grid_sidelength_);

  // special case: line is vertical
  if (std::floor(x0) == std::floor(x1)) {
    // simply insert all grid cells on that vertical line
    for (float z = std::floor(std::min(z0, z1));
         z <= std::floor(std::max(z0, z1)); z += 1.f) {
      indices.push_back(GetAccGridIndex(static_cast<unsigned int>(x0),
                                        static_cast<unsigned int>(z)));
    }
    return indices;
  }

  // reorder, x0 should < x1
  if (x0 > x1) {
    std::swap(x0, x1);
    std::swap(z0, z1);
  }

  float slope = (z1 - z0) / (x1 - x0);

  // slope <= 1: go along x (x increases more often than z)
  if (std::abs(slope) <= 1.f) {
    // add grid cells of start and end point
    indices.push_back(GetAccGridIndex(static_cast<unsigned int>(x0),
                                      static_cast<unsigned int>(z0)));
    indices.push_back(GetAccGridIndex(static_cast<unsigned int>(x1),
                                      static_cast<unsigned int>(z1)));

    // now check epsilon left and right of all cell borders
    for (float x = std::ceil(x0); x <= std::floor(x1); ++x) {
      float xleft = x - 0.001f;
      float xright = x + 0.001f;

      float z = z0 + slope * (x - x0);

      indices.push_back(GetAccGridIndex(static_cast<unsigned int>(xleft),
                                        static_cast<unsigned int>(z)));
      indices.push_back(GetAccGridIndex(static_cast<unsigned int>(xright),
                                        static_cast<unsigned int>(z)));
    }
  } else {
    // go along z

    // reorder, z0 should < z1
    if (z0 > z1) {
      std::swap(x0, x1);
      std::swap(z0, z1);
    }

    // add grid cells of start and end point
    indices.push_back(GetAccGridIndex(static_cast<unsigned int>(x0),
                                      static_cast<unsigned int>(z0)));
    indices.push_back(GetAccGridIndex(static_cast<unsigned int>(x1),
                                      static_cast<unsigned int>(z1)));

    slope = (x1 - x0) / (z1 - z0);

    // now check epsilon below and above all cell borders
    for (float z = std::ceil(z0); z <= std::floor(z1); ++z) {
      float zbelow = z - 0.001f;
      float zabove = z + 0.001f;

      float x = x0 + slope * (z - z0);

      indices.push_back(GetAccGridIndex(static_cast<unsigned int>(x),
                                        static_cast<unsigned int>(zbelow)));
      indices.push_back(GetAccGridIndex(static_cast<unsigned int>(x),
                                        static_cast<unsigned int>(zabove)));
    }
  }

  // remove duplicates in vecIndices
  std::sort(indices.begin(), indices.end());
  indices.erase(std::unique(indices.begin(), indices.end()), indices.end());

  return indices;
}

void Navmesh::InsertEdgeIntoAccGrid(unsigned int vertex_idx1,
                                    unsigned int vertex_idx2) {
  // get all triangles incident to this edge
  std::vector<unsigned int> incident_triangles =
      GetIncidentTriangles(vertex_idx1, vertex_idx2);
  if (incident_triangles.empty()) return;

  // these 1 or (usually) 2 triangles are entered into all grid cells
  // on the way between vertex_idx1 and vertex_idx2

  // trivial case: both start and end vertex are in the same grid cell:
  // then the whole edge is also in that cell, done.
  glm::uvec2 coords1 = GetAccGridCoords(vertices_[vertex_idx1]);
  glm::uvec2 coords2 = GetAccGridCoords(vertices_[vertex_idx2]);

  std::size_t grid_idx1 = GetAccGridIndex(coords1[0], coords1[1]);
  std::size_t grid_idx2 = GetAccGridIndex(coords2[0], coords2[1]);

  if (grid_idx1 == grid_idx2) {
    // all triangle ids to be entered into this grid cell
    for (unsigned int triangle_idx : incident_triangles) {
      InsertTriangleIdxIntoAccGrid(grid_idx1, triangle_idx);
    }
    // done.
    return;
  }

  // okay, different cells.
  // Find all cells that the edge traverses
  auto grid_indices = GetAccGridIndicesAlongEdge(vertices_[vertex_idx1],
                                                 vertices_[vertex_idx2]);
  // for all triangles incident to the edge, store all grid indices traversed
  for (unsigned int triangle_idx : incident_triangles) {
    for (std::size_t n : grid_indices) {
      InsertTriangleIdxIntoAccGrid(n, triangle_idx);
    }
  }
}

void Navmesh::InsertTriangleIdxIntoAccGrid(std::size_t grid_index,
                                           unsigned int triangle_idx) {
  // insert if not already contained
  auto it =
      std::find(acc_grid_triangles_by_cell_[grid_index].begin(),
                acc_grid_triangles_by_cell_[grid_index].end(), triangle_idx);

  if (it == acc_grid_triangles_by_cell_[grid_index].end()) {
    acc_grid_triangles_by_cell_[grid_index].push_back(triangle_idx);
    acc_grid_cells_by_triangle_[triangle_idx].push_back(grid_index);
  }
}

glm::uvec4 Navmesh::GetTriangleAccGridBoundingBox(
    unsigned int triangle_idx) const {
  // get coordinates of the tree vertices
  auto c1 = GetAccGridCoords(vertices_[triangles_[triangle_idx].x]);
  auto c2 = GetAccGridCoords(vertices_[triangles_[triangle_idx].y]);
  auto c3 = GetAccGridCoords(vertices_[triangles_[triangle_idx].z]);

  // determine mins / maxes
  return glm::uvec4(std::min(std::min(c1.x, c2.x), c3.x),
                    std::max(std::max(c1.x, c2.x), c3.x),
                    std::min(std::min(c1.y, c2.y), c3.y),
                    std::max(std::max(c1.y, c2.y), c3.y));
}

void Navmesh::InsertTriangleIntoAccGridCellsTotallyCoveredByIt(
    unsigned int triangle_idx) {
  // note: this is not a particularly fast way to do this, but it suffices

  // only check triangles that are large enough
  const float grid_cell_area = acc_grid_sidelength_ * acc_grid_sidelength_;
  if (GetTriangleArea(triangle_idx) > grid_cell_area) {
    // check all grid cells in the bounding box of the triangle...
    glm::uvec4 bb = GetTriangleAccGridBoundingBox(triangle_idx);

    for (unsigned int z = bb[2]; z <= bb[3]; ++z) {
      for (unsigned int x = bb[0]; x <= bb[1]; ++x) {
        // compute center of grid cell (above mesh)
        const glm::vec3 cell_center(
            (static_cast<float>(x) + 0.5f) * acc_grid_sidelength_ +
                bounds_.first[0],
            bounds_.second[1] + 1.f,
            (static_cast<float>(z) + 0.5f) * acc_grid_sidelength_ +
                bounds_.first[2]);
        // do a triangle intersection test to find out if the grid cell is on
        // the triangle or not
        glm::vec3 bary_pos;
        bool inside = glm::intersectRayTriangle(
            cell_center, glm::vec3(0.f, -1.f, 0.f),
            vertices_[triangles_[triangle_idx].x],
            vertices_[triangles_[triangle_idx].y],
            vertices_[triangles_[triangle_idx].z], bary_pos);

        if (inside) {
          // cool. add the index to the list
          InsertTriangleIdxIntoAccGrid(GetAccGridIndex(x, z), triangle_idx);
        }
      }
    }
  }
}

void Navmesh::BuildAccelerationStructure() {
  if (adjacency_structure_dirty_) BuildAdjacencyStructure();
  // heuristic: use about as many grid cells as triangles
  // (seems to be about fastest experimentally)
  glm::vec2 total_sidelengths_2d(bounds_.second.x - bounds_.first.x,
                                 bounds_.second.z - bounds_.first.z);
  float ground_area = total_sidelengths_2d[0] * total_sidelengths_2d[1];
  float area_per_cell = ground_area / static_cast<float>(triangles_.size());
  acc_grid_sidelength_ = std::sqrt(area_per_cell);

  acc_grid_dims_[0] = static_cast<unsigned int>(std::ceil(
      (bounds_.second.x - bounds_.first.x) / acc_grid_sidelength_ + 0.01f));
  acc_grid_dims_[1] = static_cast<unsigned int>(std::ceil(
      (bounds_.second.z - bounds_.first.z) / acc_grid_sidelength_ + 0.01f));

  acc_grid_triangles_by_cell_.clear();
  acc_grid_triangles_by_cell_.resize(acc_grid_dims_[0] * acc_grid_dims_[1]);
  acc_grid_cells_by_triangle_.clear();
  acc_grid_cells_by_triangle_.resize(triangles_.size());

  // account for all grid cells crossed by an edge
  for (unsigned int vertex_idx = 0; vertex_idx < vertices_.size();
       ++vertex_idx) {
    for (unsigned int neighbor_idx : edges_[vertex_idx]) {
      // only consider each edge once
      if (vertex_idx < neighbor_idx) {
        InsertEdgeIntoAccGrid(vertex_idx, neighbor_idx);
      }
    }
  }

  // all grid cells crossed by edges are done
  // missing: only grid cells that lie completely within a triangle
  for (unsigned int triangle_idx = 0; triangle_idx < triangles_.size();
       ++triangle_idx) {
    InsertTriangleIntoAccGridCellsTotallyCoveredByIt(triangle_idx);
  }

  // now sort structures for a deterministic outcome: always check the triangles
  // with the lower index first
  for (auto& vec : acc_grid_triangles_by_cell_)
    std::sort(vec.begin(), vec.end());
  for (auto& vec : acc_grid_cells_by_triangle_)
    std::sort(vec.begin(), vec.end());

  acceleration_structure_dirty_ = false;
}

}  // namespace vq
