//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_VQ_STATIC_GRID_HPP_
#define LIBRARY_VQ_STATIC_GRID_HPP_

#include <algorithm>
#include <cassert>
#include <memory>
#include <vector>

#include "vq/data_provider.hpp"
#include "vq/export.hpp"
#include "vq/interpolator.hpp"

namespace vq {

template <typename T>
class VQ_EXPORT StaticGrid : public DataProvider<T> {
 public:
  StaticGrid() = delete;
  StaticGrid(const glm::vec3& min_, const glm::vec3& spacing_,
             const glm::uvec3& dimensions_);
  virtual ~StaticGrid() = default;
  StaticGrid(const StaticGrid&) = default;
  StaticGrid(StaticGrid&&) = default;

  StaticGrid& operator=(const StaticGrid&) = default;
  StaticGrid& operator=(StaticGrid&&) = default;

  T GetDataPoint(
      const glm::vec3& pos,
      AccessStrategy strategy = AccessStrategy::NEAREST_NEIGHBOR) override;
  void SetDataPoint(const glm::vec3& pos, T data) override;

  inline T GetDataDirect(std::size_t x, std::size_t y, std::size_t z) const;

  inline const glm::vec3& GetMin() const;
  inline glm::vec3 GetMax() const;
  inline const glm::vec3& GetSpacing() const;
  inline const glm::uvec3& GetDimensions() const;

 private:
  inline std::size_t GetIndex(const glm::uvec3& coords) const;
  inline std::size_t GetIndex(std::size_t x, std::size_t y,
                              std::size_t z) const;
  inline std::size_t GetIndexNN(const glm::vec3& pos) const;

  glm::vec3 min_;
  glm::vec3 spacing_;
  glm::uvec3 dimensions_;

  std::vector<T> data_;
};

class VisibilityHistogram;

using StaticGridVQ = StaticGrid<float>;
using StaticGridVis = StaticGrid<std::shared_ptr<VisibilityHistogram>>;

template <typename T>
T StaticGrid<T>::GetDataDirect(std::size_t x, std::size_t y,
                               std::size_t z) const {
  return data_[GetIndex(x, y, z)];
}

template <typename T>
const glm::uvec3& StaticGrid<T>::GetDimensions() const {
  return dimensions_;
}

template <typename T>
const glm::vec3& StaticGrid<T>::GetSpacing() const {
  return spacing_;
}

template <typename T>
glm::vec3 StaticGrid<T>::GetMax() const {
  glm::vec3 max;
  max.x = min_.x + spacing_.x * (dimensions_.x - 1);
  max.y = min_.y + spacing_.y * (dimensions_.y - 1);
  max.z = min_.z + spacing_.z * (dimensions_.z - 1);
  return max;
}

template <typename T>
const glm::vec3& StaticGrid<T>::GetMin() const {
  return min_;
}

template <typename T>
void StaticGrid<T>::SetDataPoint(const glm::vec3& pos, T data) {
  // set
  data_[GetIndexNN(pos)] = data;
}

template <typename T>
std::size_t StaticGrid<T>::GetIndex(const glm::uvec3& coords) const {
  return GetIndex(coords.x, coords.y, coords.z);
}

template <typename T>
std::size_t StaticGrid<T>::GetIndex(std::size_t x, std::size_t y,
                                    std::size_t z) const {
  return z * dimensions_[0] * dimensions_[1] + y * dimensions_[0] + x;
}

template <typename T>
std::size_t StaticGrid<T>::GetIndexNN(const glm::vec3& pos) const {
  // compute position in data space coordinates
  glm::vec3 coords = (pos - min_) / spacing_;
  // round to next coordinate (NN strategy)
  std::size_t x = std::min(
      static_cast<std::size_t>(dimensions_[0] - 1),
      static_cast<std::size_t>(std::max(0.f, (std::round(coords[0])))));
  std::size_t y = std::min(
      static_cast<std::size_t>(dimensions_[1] - 1),
      static_cast<std::size_t>(std::max(0.f, (std::round(coords[1])))));
  std::size_t z = std::min(
      static_cast<std::size_t>(dimensions_[2] - 1),
      static_cast<std::size_t>(std::max(0.f, (std::round(coords[2])))));
  return GetIndex(x, y, z);
}

template <typename T>
StaticGrid<T>::StaticGrid(const glm::vec3& min, const glm::vec3& spacing,
                          const glm::uvec3& dimensions)
    : min_(min), spacing_(spacing), dimensions_(dimensions) {
  assert(dimensions[0] > 0);
  assert(dimensions[1] > 0);
  assert(dimensions[2] > 0);
  assert(spacing[0] > 0.f);
  assert(spacing[1] > 0.f);
  assert(spacing[2] > 0.f);

  data_.resize(dimensions[0] * dimensions[1] * dimensions[2]);
}

template <typename T>
T StaticGrid<T>::GetDataPoint(const glm::vec3& pos, AccessStrategy strategy) {
  if (strategy == AccessStrategy::NEAREST_NEIGHBOR) {
    return data_[GetIndexNN(pos)];
  } else if (strategy == AccessStrategy::LINEAR_INTERPOLATION) {
    Interpolator<T, StaticGrid> interpolator;
    return interpolator.Interpolate(pos, this);
  }

  return T();
}

}  // namespace vq

#endif  // LIBRARY_VQ_STATIC_GRID_HPP_
