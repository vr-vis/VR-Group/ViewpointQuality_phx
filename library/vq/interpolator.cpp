//------------------------------------------------------------------------------
// Viewpoint Quality
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "interpolator.hpp"

#include <glm/gtx/intersect.hpp>

#include <cmath>
#include <limits>
#include <memory>

#include "navmesh_data_provider.hpp"
#include "static_grid.hpp"
#include "visibility_histogram.hpp"

namespace vq {

template <>
float Interpolator<float, StaticGrid<float>>::Interpolate(
    const glm::vec3& pos, StaticGrid<float>* data_provider) const {
  // compute position in data space coordinates
  const glm::vec3 coords =
      (pos - data_provider->GetMin()) / data_provider->GetSpacing();
  const glm::uvec3 coords_lower(
      static_cast<unsigned int>(std::floor(coords.x)),
      static_cast<unsigned int>(std::floor(coords.y)),
      static_cast<unsigned int>(std::floor(coords.z)));
  const glm::uvec3 coords_higher(
      static_cast<unsigned int>(std::ceil(coords.x)),
      static_cast<unsigned int>(std::ceil(coords.y)),
      static_cast<unsigned int>(std::ceil(coords.z)));

  const glm::vec3 frac(coords.x - std::floor(coords.x),
                       coords.y - std::floor(coords.y),
                       coords.z - std::floor(coords.z));

  float values[2][2][2];
  values[0][0][0] = data_provider->GetDataDirect(coords_lower.x, coords_lower.y,
                                                 coords_lower.z);
  values[1][0][0] = data_provider->GetDataDirect(
      coords_higher.x, coords_lower.y, coords_lower.z);
  values[0][1][0] = data_provider->GetDataDirect(
      coords_lower.x, coords_higher.y, coords_lower.z);
  values[1][1][0] = data_provider->GetDataDirect(
      coords_higher.x, coords_higher.y, coords_lower.z);
  values[0][0][1] = data_provider->GetDataDirect(coords_lower.x, coords_lower.y,
                                                 coords_higher.z);
  values[1][0][1] = data_provider->GetDataDirect(
      coords_higher.x, coords_lower.y, coords_higher.z);
  values[0][1][1] = data_provider->GetDataDirect(
      coords_lower.x, coords_higher.y, coords_higher.z);
  values[1][1][1] = data_provider->GetDataDirect(
      coords_higher.x, coords_higher.y, coords_higher.z);

  float c00 = values[0][0][0] * (1 - frac.x) + values[1][0][0] * frac.x;
  float c10 = values[0][1][0] * (1 - frac.x) + values[1][1][0] * frac.x;
  float c01 = values[0][0][1] * (1 - frac.x) + values[1][0][1] * frac.x;
  float c11 = values[0][1][1] * (1 - frac.x) + values[1][1][1] * frac.x;

  float c0 = c00 * (1 - frac.y) + c10 * frac.y;
  float c1 = c01 * (1 - frac.y) + c11 * frac.y;

  return c0 * (1 - frac.z) + c1 * frac.z;
}

template <>
std::shared_ptr<VisibilityHistogram>
Interpolator<std::shared_ptr<VisibilityHistogram>, StaticGridVis>::Interpolate(
    const glm::vec3& pos, StaticGridVis* data_provider) const {
  // compute position in data space coordinates
  const glm::vec3 coords =
      (pos - data_provider->GetMin()) / data_provider->GetSpacing();
  const glm::uvec3 coords_lower(
      static_cast<unsigned int>(std::floor(coords.x)),
      static_cast<unsigned int>(std::floor(coords.y)),
      static_cast<unsigned int>(std::floor(coords.z)));
  const glm::uvec3 coords_higher(
      static_cast<unsigned int>(std::ceil(coords.x)),
      static_cast<unsigned int>(std::ceil(coords.y)),
      static_cast<unsigned int>(std::ceil(coords.z)));

  const glm::vec3 frac(coords.x - std::floor(coords.x),
                       coords.y - std::floor(coords.y),
                       coords.z - std::floor(coords.z));

  std::shared_ptr<VisibilityHistogram> values[2][2][2];
  values[0][0][0] = data_provider->GetDataDirect(coords_lower.x, coords_lower.y,
                                                 coords_lower.z);
  values[1][0][0] = data_provider->GetDataDirect(
      coords_higher.x, coords_lower.y, coords_lower.z);
  values[0][1][0] = data_provider->GetDataDirect(
      coords_lower.x, coords_higher.y, coords_lower.z);
  values[1][1][0] = data_provider->GetDataDirect(
      coords_higher.x, coords_higher.y, coords_lower.z);
  values[0][0][1] = data_provider->GetDataDirect(coords_lower.x, coords_lower.y,
                                                 coords_higher.z);
  values[1][0][1] = data_provider->GetDataDirect(
      coords_higher.x, coords_lower.y, coords_higher.z);
  values[0][1][1] = data_provider->GetDataDirect(
      coords_lower.x, coords_higher.y, coords_higher.z);
  values[1][1][1] = data_provider->GetDataDirect(
      coords_higher.x, coords_higher.y, coords_higher.z);

  auto c00 = VisibilityHistogram::GetMixedIgnoreNull(
      values[0][0][0].get(), (1 - frac.x), values[1][0][0].get());
  auto c10 = VisibilityHistogram::GetMixedIgnoreNull(
      values[0][1][0].get(), (1 - frac.x), values[1][1][0].get());
  auto c01 = VisibilityHistogram::GetMixedIgnoreNull(
      values[0][0][1].get(), (1 - frac.x), values[1][0][1].get());
  auto c11 = VisibilityHistogram::GetMixedIgnoreNull(
      values[0][1][1].get(), (1 - frac.x), values[1][1][1].get());

  auto c0 = VisibilityHistogram::GetMixedIgnoreNull(c00.get(), (1 - frac.y),
                                                    c10.get());
  auto c1 = VisibilityHistogram::GetMixedIgnoreNull(c01.get(), (1 - frac.y),
                                                    c11.get());

  return VisibilityHistogram::GetMixedIgnoreNull(c0.get(), (1 - frac.z),
                                                 c1.get());
}

template <>
float Interpolator<float, NavMeshDataProviderVQ>::Interpolate(
    const glm::vec3& pos, NavMeshDataProviderVQ* data_provider) const {
  unsigned int triangle_idx;
  glm::vec3 proj_pos_on_triangle;
  if (!data_provider->GetNavmesh()->GetTriangleBelowPosition(
          pos, triangle_idx, proj_pos_on_triangle)) {
    if (!data_provider->GetNavmesh()->GetClosestTriangleEdgeBelowPosition(
            pos, triangle_idx, proj_pos_on_triangle)) {
      // cannot find anything
      return std::numeric_limits<float>::quiet_NaN();
    }
  }

  // compute barycentric coordinates on resulting triangle
  auto triangle = data_provider->GetNavmesh()->GetTriangle(triangle_idx);
  glm::vec3 barycentric;
  glm::intersectLineTriangle(proj_pos_on_triangle, glm::vec3(0.f, 1.f, 0.f),
                             data_provider->GetNavmesh()->GetVertex(triangle.x),
                             data_provider->GetNavmesh()->GetVertex(triangle.y),
                             data_provider->GetNavmesh()->GetVertex(triangle.z),
                             barycentric);
  barycentric.x = 1.f - barycentric.y - barycentric.z;

  // get values at vertices
  float val_x = data_provider->GetDataDirect(triangle.x);
  float val_y = data_provider->GetDataDirect(triangle.y);
  float val_z = data_provider->GetDataDirect(triangle.z);

  // mix according to barycentric coordinates
  return barycentric.x * val_x + barycentric.y * val_y + barycentric.z * val_z;
}

template <>
std::shared_ptr<VisibilityHistogram>
Interpolator<std::shared_ptr<VisibilityHistogram>,
             NavMeshDataProviderVis>::Interpolate(const glm::vec3& pos,
                                                  NavMeshDataProviderVis*
                                                      data_provider) const {
  unsigned int triangle_idx;
  glm::vec3 proj_pos_on_triangle;
  if (!data_provider->GetNavmesh()->GetTriangleBelowPosition(
          pos, triangle_idx, proj_pos_on_triangle)) {
    if (!data_provider->GetNavmesh()->GetClosestTriangleEdgeBelowPosition(
            pos, triangle_idx, proj_pos_on_triangle)) {
      // cannot find anything
      return nullptr;
    }
  }

  // compute barycentric coordinates on resulting triangle
  auto triangle = data_provider->GetNavmesh()->GetTriangle(triangle_idx);
  glm::vec3 barycentric;
  glm::intersectLineTriangle(proj_pos_on_triangle, glm::vec3(0.f, 1.f, 0.f),
                             data_provider->GetNavmesh()->GetVertex(triangle.x),
                             data_provider->GetNavmesh()->GetVertex(triangle.y),
                             data_provider->GetNavmesh()->GetVertex(triangle.z),
                             barycentric);
  barycentric.x = 1.f - barycentric.y - barycentric.z;

  // get histograms at vertices
  auto hist_x = data_provider->GetDataDirect(triangle.x);
  auto hist_y = data_provider->GetDataDirect(triangle.y);
  auto hist_z = data_provider->GetDataDirect(triangle.z);

  // mix according to barycentric coordinates
  return VisibilityHistogram::GetMixedIgnoreNull(
      hist_x.get(), barycentric.x, hist_y.get(), barycentric.y, hist_z.get());
}

}  // namespace vq
