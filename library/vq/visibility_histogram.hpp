//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_VQ_VISIBILITY_HISTOGRAM_HPP_
#define LIBRARY_VQ_VISIBILITY_HISTOGRAM_HPP_

#include <functional>
#include <memory>
#include <utility>
#include <vector>

#include "vq/export.hpp"

namespace vq {

class VQ_EXPORT VisibilityHistogram {
 public:
  VisibilityHistogram() = default;
  virtual ~VisibilityHistogram() = default;
  VisibilityHistogram(const VisibilityHistogram&) = default;
  VisibilityHistogram(VisibilityHistogram&&) = default;

  VisibilityHistogram& operator=(const VisibilityHistogram&) = default;
  VisibilityHistogram& operator=(VisibilityHistogram&&) = default;

  float Get(unsigned int entity_idx) const;
  void Set(unsigned int entity_idx, float visual_size);

  unsigned int GetMaxEntity() const;
  float GetSum() const;

  void Normalize();
  bool GetNormalized() const;

  unsigned int GetNumNonZeroEntries();

  void ApplyToAllNonZeroEntries(const std::function<float(float)>& func);
  void ApplyToAllNonZeroEntries(
      const std::function<float(unsigned int, float)>& func);

  std::shared_ptr<VisibilityHistogram> GetMixed(
      const VisibilityHistogram* other, float weight_other = 0.5f) const;
  float ComputeHistogramIntersection(const VisibilityHistogram* other) const;
  float ComputeBhattacharyyaCoefficient(const VisibilityHistogram* other) const;

  bool Equals(const VisibilityHistogram* other) const;
  bool Equals(const std::shared_ptr<VisibilityHistogram>& other) const;
  std::shared_ptr<VisibilityHistogram> Copy() const;

  static std::shared_ptr<VisibilityHistogram> GetMixed(
      const VisibilityHistogram* first, float weight_first,
      const VisibilityHistogram* second);
  static std::shared_ptr<VisibilityHistogram> GetMixed(
      const std::shared_ptr<VisibilityHistogram>& first, float weight_first,
      const std::shared_ptr<VisibilityHistogram>& second);

  static std::shared_ptr<VisibilityHistogram> GetMixed(
      const VisibilityHistogram* first, float weight_first,
      const VisibilityHistogram* second, float weight_second,
      const VisibilityHistogram* third);
  static std::shared_ptr<VisibilityHistogram> GetMixed(
      const std::shared_ptr<VisibilityHistogram>& first, float weight_first,
      const std::shared_ptr<VisibilityHistogram>& second, float weight_second,
      const std::shared_ptr<VisibilityHistogram>& third);

  static std::shared_ptr<VisibilityHistogram> GetMixedIgnoreNull(
      const VisibilityHistogram* first, float weight_first,
      const VisibilityHistogram* second);
  static std::shared_ptr<VisibilityHistogram> GetMixedIgnoreNull(
      const std::shared_ptr<VisibilityHistogram>& first, float weight_first,
      const std::shared_ptr<VisibilityHistogram>& second);

  static std::shared_ptr<VisibilityHistogram> GetMixedIgnoreNull(
      const VisibilityHistogram* first, float weight_first,
      const VisibilityHistogram* second, float weight_second,
      const VisibilityHistogram* third);
  static std::shared_ptr<VisibilityHistogram> GetMixedIgnoreNull(
      const std::shared_ptr<VisibilityHistogram>& first, float weight_first,
      const std::shared_ptr<VisibilityHistogram>& second, float weight_second,
      const std::shared_ptr<VisibilityHistogram>& third);

  static float ComputeHistogramIntersection(const VisibilityHistogram* first,
                                            const VisibilityHistogram* second);
  static float ComputeHistogramIntersection(const VisibilityHistogram* first,
                                            const VisibilityHistogram* second,
                                            const VisibilityHistogram* third);
  static float ComputeHistogramIntersection(
      const std::shared_ptr<VisibilityHistogram>& first,
      const std::shared_ptr<VisibilityHistogram>& second);
  static float ComputeHistogramIntersection(
      const std::shared_ptr<VisibilityHistogram>& first,
      const std::shared_ptr<VisibilityHistogram>& second,
      const std::shared_ptr<VisibilityHistogram>& third);

  static float ComputeBhattacharyyaCoefficient(
      const VisibilityHistogram* first, const VisibilityHistogram* second);

  std::vector<std::pair<unsigned int, float>>::const_iterator begin() const;
  std::vector<std::pair<unsigned int, float>>::const_iterator end() const;

 private:
  using VisEntry = std::pair<unsigned int, float>;
  std::vector<VisEntry> vis_data_;
};

}  // namespace vq

#endif  // LIBRARY_VQ_VISIBILITY_HISTOGRAM_HPP_
