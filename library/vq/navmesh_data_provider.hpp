//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_VQ_NAVMESH_DATA_PROVIDER_HPP_
#define LIBRARY_VQ_NAVMESH_DATA_PROVIDER_HPP_

#include <algorithm>
#include <cassert>
#include <memory>
#include <vector>

#include "vq/data_provider.hpp"
#include "vq/export.hpp"
#include "vq/interpolator.hpp"
#include "vq/navmesh.hpp"

namespace vq {

template <typename T>
class VQ_EXPORT NavMeshDataProvider : public DataProvider<T> {
 public:
  NavMeshDataProvider() = delete;
  explicit NavMeshDataProvider(std::shared_ptr<Navmesh> navmesh);
  virtual ~NavMeshDataProvider() = default;
  NavMeshDataProvider(const NavMeshDataProvider&) = default;
  NavMeshDataProvider(NavMeshDataProvider&&) = default;

  NavMeshDataProvider& operator=(const NavMeshDataProvider&) = default;
  NavMeshDataProvider& operator=(NavMeshDataProvider&&) = default;

  T GetDataPoint(
      const glm::vec3& pos,
      AccessStrategy strategy = AccessStrategy::NEAREST_NEIGHBOR) override;
  void SetDataPoint(const glm::vec3& pos, T data) override;

  inline T GetDataDirect(unsigned int vertex_idx) const;

  inline std::shared_ptr<Navmesh> GetNavmesh() const;

 private:
  unsigned int GetIndexNN(const glm::vec3& pos) const;

  std::vector<T> data_;
  std::shared_ptr<Navmesh> navmesh_;
};

class VisibilityHistogram;

using NavMeshDataProviderVQ = NavMeshDataProvider<float>;
using NavMeshDataProviderVis =
    NavMeshDataProvider<std::shared_ptr<VisibilityHistogram>>;

template <typename T>
vq::NavMeshDataProvider<T>::NavMeshDataProvider(
    std::shared_ptr<Navmesh> navmesh)
    : navmesh_(navmesh) {
  assert(navmesh != nullptr);
  data_.resize(navmesh->GetNumVertices());
}

template <typename T>
T NavMeshDataProvider<T>::GetDataDirect(unsigned int vertex_idx) const {
  return data_[vertex_idx];
}

template <typename T>
void NavMeshDataProvider<T>::SetDataPoint(const glm::vec3& pos, T data) {
  // set
  data_[GetIndexNN(pos)] = data;
}

template <typename T>
unsigned int NavMeshDataProvider<T>::GetIndexNN(const glm::vec3& pos) const {
  unsigned int vertex_idx;
  if (navmesh_->GetClosestVertexOfClosestTriangleBelowPosition(pos, vertex_idx))
    return vertex_idx;

  return ~0;
}

template <typename T>
T NavMeshDataProvider<T>::GetDataPoint(const glm::vec3& pos,
                                       AccessStrategy strategy) {
  if (strategy == AccessStrategy::NEAREST_NEIGHBOR) {
    return data_[GetIndexNN(pos)];
  } else if (strategy == AccessStrategy::LINEAR_INTERPOLATION) {
    Interpolator<T, NavMeshDataProvider> interpolator;
    return interpolator.Interpolate(pos, this);
  }

  return T();
}

template <typename T>
std::shared_ptr<vq::Navmesh> vq::NavMeshDataProvider<T>::GetNavmesh() const {
  return navmesh_;
}

}  // namespace vq

#endif  // LIBRARY_VQ_NAVMESH_DATA_PROVIDER_HPP_
