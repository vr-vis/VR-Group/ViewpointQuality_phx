//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_VQ_ENTROPY_HPP_
#define LIBRARY_VQ_ENTROPY_HPP_

#include "vq/export.hpp"
#include "vq/viewpoint_quality_estimator.hpp"

namespace vq {

class VQ_EXPORT Entropy : public ViewpointQualityEstimator {
 public:
  float GetViewpointQuality(const VisibilityHistogram* visibility) override;

  void SetIncludeBackground(bool include_background);
  bool GetIncludeBackground() const;

  void SetExponential(bool exponential);
  bool GetExponential() const;

 private:
  bool include_background_ = false;
  bool exponential_ = false;
};

}  // namespace vq

#endif  // LIBRARY_VQ_ENTROPY_HPP_
