//------------------------------------------------------------------------------
// Viewpoint Quality
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "visibility_histogram.hpp"

#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>
#include <utility>
#include <vector>

namespace vq {
float VisibilityHistogram::Get(unsigned int entity_idx) const {
  const VisEntry entry = std::make_pair(entity_idx, 0.f);
  auto it = std::lower_bound(vis_data_.begin(), vis_data_.end(), entry);
  if (it != vis_data_.end() && it->first == entity_idx) return it->second;
  return 0.f;
}

void VisibilityHistogram::Set(unsigned int entity_idx, float visual_size) {
  // find entry
  const VisEntry entry = std::make_pair(entity_idx, visual_size);
  auto it = std::lower_bound(vis_data_.begin(), vis_data_.end(), entry);
  if (it != vis_data_.end() && it->first == entity_idx) {
    // entry exists, change it
    it->second = visual_size;
  } else {
    // insert new entry before the one we found
    vis_data_.insert(it, entry);
  }
}

unsigned int VisibilityHistogram::GetMaxEntity() const {
  if (vis_data_.empty()) return 0;
  return vis_data_.back().first;
}

float VisibilityHistogram::GetSum() const {
  float sum = 0.f;
  for (const auto& entry : vis_data_) sum += entry.second;
  return sum;
}

void VisibilityHistogram::Normalize() {
  const float sum = GetSum();
  if (std::abs(sum - 1.f) < 10.f * std::numeric_limits<float>::epsilon())
    return;
  for (auto& entry : vis_data_) entry.second /= sum;
}

bool VisibilityHistogram::GetNormalized() const {
  const float sum = GetSum();
  return (std::abs(sum - 1.f) < 10.f * std::numeric_limits<float>::epsilon());
}

unsigned int VisibilityHistogram::GetNumNonZeroEntries() {
  // remove zero-entries first
  vis_data_.erase(
      std::remove_if(vis_data_.begin(), vis_data_.end(),
                     [](const VisEntry& entry) { return entry.second == 0.f; }),
      vis_data_.end());
  return static_cast<unsigned int>(vis_data_.size());
}

void VisibilityHistogram::ApplyToAllNonZeroEntries(
    const std::function<float(float)>& func) {
  auto func_full = [&func](const VisEntry& entry) {
    return std::make_pair(entry.first, func(entry.second));
  };

  std::transform(vis_data_.begin(), vis_data_.end(), vis_data_.begin(),
                 func_full);
}

void VisibilityHistogram::ApplyToAllNonZeroEntries(
    const std::function<float(unsigned int, float)>& func) {
  auto func_full = [&func](const VisEntry& entry) {
    return std::make_pair(entry.first, func(entry.first, entry.second));
  };

  std::transform(vis_data_.begin(), vis_data_.end(), vis_data_.begin(),
                 func_full);
}

std::shared_ptr<vq::VisibilityHistogram> VisibilityHistogram::GetMixed(
    const VisibilityHistogram* other, float weight_other /*= 0.5f*/) const {
  return GetMixed(this, 1.f - weight_other, other);
}

std::shared_ptr<vq::VisibilityHistogram> VisibilityHistogram::GetMixed(
    const VisibilityHistogram* first, float weight_first,
    const VisibilityHistogram* second) {
  if (first == nullptr || second == nullptr) return nullptr;

  // if weight is 0 or 1, just copy
  if (weight_first == 0.f) {
    return std::make_shared<vq::VisibilityHistogram>(*second);
  }
  if (weight_first == 1.f) {
    return std::make_shared<vq::VisibilityHistogram>(*first);
  }

  auto result = std::make_shared<vq::VisibilityHistogram>();

  result->vis_data_.reserve(
      std::max(first->vis_data_.size(), second->vis_data_.size()));

  // first: the interval where both have non-zero values
  auto it_this = first->vis_data_.begin();
  auto it_second = second->vis_data_.begin();
  while (it_this != first->vis_data_.end() &&
         it_second != second->vis_data_.end()) {
    // both in the same bin: mix
    if (it_this->first == it_second->first) {
      result->vis_data_.push_back(std::make_pair(
          it_this->first, weight_first * it_this->second +
                              (1.f - weight_first) * it_second->second));
      ++it_this;
      ++it_second;
    } else {
      // both in different bins: count the one with larger entity idx as 0,
      // increase only smaller one
      if (it_this->first < it_second->first) {
        result->vis_data_.push_back(
            std::make_pair(it_this->first, weight_first * it_this->second));
        ++it_this;
      } else {
        result->vis_data_.push_back(std::make_pair(
            it_second->first, (1.f - weight_first) * it_second->second));
        ++it_second;
      }
    }
  }

  // now the rest of the larger histogram, just mix the values with 0
  while (it_this != first->vis_data_.end()) {
    result->vis_data_.push_back(
        std::make_pair(it_this->first, weight_first * it_this->second));
    ++it_this;
  }
  while (it_second != second->vis_data_.end()) {
    result->vis_data_.push_back(std::make_pair(
        it_second->first, (1.f - weight_first) * it_second->second));
    ++it_second;
  }

  return result;
}

std::shared_ptr<vq::VisibilityHistogram> VisibilityHistogram::GetMixed(
    const VisibilityHistogram* first, float weight_first,
    const VisibilityHistogram* second, float weight_second,
    const VisibilityHistogram* third) {
  // if a weight is 0, revert to 2-way case
  if (weight_first == 0.f) return GetMixed(second, weight_second, third);
  if (weight_second == 0.f) return GetMixed(first, weight_first, third);
  if (weight_first + weight_second == 1.f)
    return GetMixed(first, weight_first, second);

  // ok, do three-wise mix
  auto result = std::make_shared<vq::VisibilityHistogram>();

  float weight_third = 1.f - weight_first - weight_second;

  auto it_first = first->vis_data_.begin();
  auto it_second = second->vis_data_.begin();
  auto it_third = third->vis_data_.begin();

  auto end_first = first->vis_data_.end();
  auto end_second = second->vis_data_.end();
  auto end_third = third->vis_data_.end();

  // we may need more space than this (in fact, we probably need more), but we
  // don't know how much more
  result->vis_data_.reserve(
      std::max(std::max(first->vis_data_.size(), second->vis_data_.size()),
               third->vis_data_.size()));

  while (it_first != end_first || it_second != end_second ||
         it_third != end_third) {
    // all in the same bin: mix
    if (it_first != end_first && it_second != end_second &&
        it_third != end_third && it_first->first == it_second->first &&
        it_first->first == it_third->first) {
      result->vis_data_.push_back(std::make_pair(
          it_first->first, weight_first * it_first->second +
                               weight_second * it_second->second +
                               weight_third * it_third->second));
      ++it_first;
      ++it_second;
      ++it_third;
    } else {
      // the two smallest ones are in the same bin: mix and increment those
      if ((it_second != end_second && it_third != end_third) &&
          (it_first == end_first || it_first->first > it_second->first) &&
          it_second->first == it_third->first) {
        result->vis_data_.push_back(std::make_pair(
            it_second->first, weight_second * it_second->second +
                                  weight_third * it_third->second));
        ++it_second;
        ++it_third;
      } else if ((it_first != end_first && it_third != end_third) &&
                 (it_second == end_second ||
                  it_second->first > it_first->first) &&
                 it_first->first == it_third->first) {
        result->vis_data_.push_back(std::make_pair(
            it_first->first,
            weight_first * it_first->second + weight_third * it_third->second));
        ++it_first;
        ++it_third;
      } else if ((it_first != end_first && it_second != end_second) &&
                 (it_third == end_third || it_third->first > it_first->first) &&
                 it_first->first == it_second->first) {
        result->vis_data_.push_back(std::make_pair(
            it_first->first, weight_first * it_first->second +
                                 weight_second * it_second->second));
        ++it_first;
        ++it_second;
      } else {
        // all in different bins: only weigh and increment the smallest one
        if (it_first != end_first &&
            (it_second == end_second || it_first->first < it_second->first) &&
            (it_third == end_third || it_first->first < it_third->first)) {
          result->vis_data_.push_back(
              std::make_pair(it_first->first, weight_first * it_first->second));
          ++it_first;
        } else if (it_second != end_second &&
                   (it_first == end_first ||
                    it_second->first < it_first->first) &&
                   (it_third == end_third ||
                    it_second->first < it_third->first)) {
          result->vis_data_.push_back(std::make_pair(
              it_second->first, weight_second * it_second->second));
          ++it_second;
        } else {
          result->vis_data_.push_back(
              std::make_pair(it_third->first, weight_third * it_third->second));
          ++it_third;
        }
      }
    }
  }

  return result;
}

std::shared_ptr<vq::VisibilityHistogram> VisibilityHistogram::GetMixed(
    const std::shared_ptr<VisibilityHistogram>& first, float weight_first,
    const std::shared_ptr<VisibilityHistogram>& second) {
  return GetMixed(first.get(), weight_first, second.get());
}

std::shared_ptr<vq::VisibilityHistogram> VisibilityHistogram::GetMixed(
    const std::shared_ptr<VisibilityHistogram>& first, float weight_first,
    const std::shared_ptr<VisibilityHistogram>& second, float weight_second,
    const std::shared_ptr<VisibilityHistogram>& third) {
  return GetMixed(first.get(), weight_first, second.get(), weight_second,
                  third.get());
}

std::shared_ptr<vq::VisibilityHistogram>
VisibilityHistogram::GetMixedIgnoreNull(const VisibilityHistogram* first,
                                        float weight_first,
                                        const VisibilityHistogram* second) {
  if (first == nullptr && second == nullptr) {
    return nullptr;
  } else if (first == nullptr) {
    return std::make_shared<vq::VisibilityHistogram>(*second);
  } else if (second == nullptr) {
    return std::make_shared<vq::VisibilityHistogram>(*first);
  }

  return GetMixed(first, weight_first, second);
}

std::shared_ptr<vq::VisibilityHistogram>
VisibilityHistogram::GetMixedIgnoreNull(const VisibilityHistogram* first,
                                        float weight_first,
                                        const VisibilityHistogram* second,
                                        float weight_second,
                                        const VisibilityHistogram* third) {
  if (first == nullptr && second == nullptr && third == nullptr) {
    return nullptr;
  } else if (first == nullptr) {
    weight_second /= (1.f - weight_first);
    return GetMixedIgnoreNull(second, weight_second, third);
  } else if (second == nullptr) {
    weight_first /= (1.f - weight_second);
    return GetMixedIgnoreNull(first, weight_first, third);
  } else if (third == nullptr) {
    float weight_third = 1.f - weight_first - weight_second;
    weight_first /= (1.f - weight_third);
    return GetMixedIgnoreNull(first, weight_first, second);
  }

  return GetMixed(first, weight_first, second, weight_second, third);
}

std::shared_ptr<vq::VisibilityHistogram>
VisibilityHistogram::GetMixedIgnoreNull(
    const std::shared_ptr<VisibilityHistogram>& first, float weight_first,
    const std::shared_ptr<VisibilityHistogram>& second) {
  return GetMixedIgnoreNull(first.get(), weight_first, second.get());
}

std::shared_ptr<vq::VisibilityHistogram>
VisibilityHistogram::GetMixedIgnoreNull(
    const std::shared_ptr<VisibilityHistogram>& first, float weight_first,
    const std::shared_ptr<VisibilityHistogram>& second, float weight_second,
    const std::shared_ptr<VisibilityHistogram>& third) {
  return GetMixedIgnoreNull(first.get(), weight_first, second.get(),
                            weight_second, third.get());
}

float VisibilityHistogram::ComputeHistogramIntersection(
    const VisibilityHistogram* first, const VisibilityHistogram* second) {
  if (first == nullptr || second == nullptr) return 0.f;

  float sum = 0.f;

  auto it_first = first->vis_data_.begin();
  auto it_second = second->vis_data_.begin();

  while (it_first != first->vis_data_.end() &&
         it_second != second->vis_data_.end()) {
    // both in the same bin: add intersection, increase both
    if (it_first->first == it_second->first) {
      sum += std::min(it_first->second, it_second->second);
      ++it_first;
      ++it_second;
    } else {
      // both in different bins: intersection here would be 0, increase the
      // smaller one
      if (it_first->first < it_second->first)
        ++it_first;
      else
        ++it_second;
    }
  }

  return sum;
}

float VisibilityHistogram::ComputeHistogramIntersection(
    const VisibilityHistogram* first, const VisibilityHistogram* second,
    const VisibilityHistogram* third) {
  float sum = 0.f;

  auto it_first = first->vis_data_.begin();
  auto it_second = second->vis_data_.begin();
  auto it_third = third->vis_data_.begin();

  while (it_first != first->vis_data_.end() &&
         it_second != second->vis_data_.end() &&
         it_third != third->vis_data_.end()) {
    // all in the same bin: add intersection, increase all
    if (it_first->first == it_second->first &&
        it_first->first == it_third->first) {
      sum += std::min(std::min(it_first->second, it_second->second),
                      it_third->second);
      ++it_first;
      ++it_second;
      ++it_third;
    } else {
      // at least one is in a different bin: the intersection is 0
      // increase the smallest index (if there are two: increase any of them,
      // that's easier then to check for all possible pairs)
      if (it_first->first <= it_second->first &&
          it_first->first <= it_third->first)
        ++it_first;
      else if (it_second->first <= it_first->first &&
               it_second->first <= it_third->first)
        ++it_second;
      else
        ++it_third;
    }
  }

  return sum;
}

float VisibilityHistogram::ComputeHistogramIntersection(
    const VisibilityHistogram* other) const {
  return VisibilityHistogram::ComputeHistogramIntersection(this, other);
}

float VisibilityHistogram::ComputeHistogramIntersection(
    const std::shared_ptr<VisibilityHistogram>& first,
    const std::shared_ptr<VisibilityHistogram>& second) {
  return ComputeHistogramIntersection(first.get(), second.get());
}

float VisibilityHistogram::ComputeHistogramIntersection(
    const std::shared_ptr<VisibilityHistogram>& first,
    const std::shared_ptr<VisibilityHistogram>& second,
    const std::shared_ptr<VisibilityHistogram>& third) {
  return ComputeHistogramIntersection(first.get(), second.get(), third.get());
}

float VisibilityHistogram::ComputeBhattacharyyaCoefficient(
    const VisibilityHistogram* first, const VisibilityHistogram* second) {
  // defined as
  // BC(p, q) = Sum( sqrt(p(x) * q(x)) )

  float sum = 0.f;

  auto it_first = first->vis_data_.begin();
  auto it_second = second->vis_data_.begin();

  while (it_first != first->vis_data_.end() &&
         it_second != second->vis_data_.end()) {
    // both in the same bin: add intersection, increase both
    if (it_first->first == it_second->first) {
      sum += std::sqrt(it_first->second * it_second->second);
      ++it_first;
      ++it_second;
    } else {
      // both in different bins: intersection here would be 0, increase the
      // smaller one
      if (it_first->first < it_second->first)
        ++it_first;
      else
        ++it_second;
    }
  }

  return sum;
}

float VisibilityHistogram::ComputeBhattacharyyaCoefficient(
    const VisibilityHistogram* other) const {
  return VisibilityHistogram::ComputeBhattacharyyaCoefficient(this, other);
}

bool VisibilityHistogram::Equals(const VisibilityHistogram* other) const {
  if (other == nullptr) return false;

  // go through entries and compare
  // note: 0-entries are allowed to exist on one side only
  auto it_this = vis_data_.begin();
  auto it_other = other->vis_data_.begin();

  while (it_this != vis_data_.end() && it_other != other->vis_data_.end()) {
    if (it_this->first != it_other->first) {
      // unequal entity idx is only allowed if the smaller idx has a visual size
      // of 0
      if (it_this->first < it_other->first) {
        if (it_this->second != 0.f) return false;
        ++it_this;
      } else if (it_this->first > it_other->first) {
        if (it_other->second != 0.f) return false;
        ++it_other;
      }
    } else {
      if (it_this->second != it_other->second) return false;
      ++it_this;
      ++it_other;
    }
  }
  // now the rest of the larger histogram, all values must be 0
  while (it_this != vis_data_.end()) {
    if (it_this->second != 0.f) return false;
    ++it_this;
  }
  while (it_other != other->vis_data_.end()) {
    if (it_other->second != 0.f) return false;
    ++it_other;
  }
  return true;
}

bool VisibilityHistogram::Equals(
    const std::shared_ptr<VisibilityHistogram>& other) const {
  return this->Equals(other.get());
}

std::shared_ptr<vq::VisibilityHistogram> VisibilityHistogram::Copy() const {
  return std::make_shared<vq::VisibilityHistogram>(*this);
}

std::vector<std::pair<unsigned int, float>>::const_iterator
VisibilityHistogram::begin() const {
  return vis_data_.begin();
}

std::vector<std::pair<unsigned int, float>>::const_iterator
VisibilityHistogram::end() const {
  return vis_data_.end();
}

}  // namespace vq
