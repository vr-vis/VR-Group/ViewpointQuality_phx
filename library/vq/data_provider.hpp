//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_VQ_DATA_PROVIDER_HPP_
#define LIBRARY_VQ_DATA_PROVIDER_HPP_

SUPPRESS_WARNINGS_BEGIN
#include <glm/glm.hpp>
SUPPRESS_WARNINGS_END

#include "vq/export.hpp"

namespace vq {

enum class VQ_EXPORT AccessStrategy { NEAREST_NEIGHBOR, LINEAR_INTERPOLATION };

template <typename T>
class VQ_EXPORT DataProvider {
 public:
  virtual T GetDataPoint(
      const glm::vec3& pos,
      AccessStrategy strategy = AccessStrategy::NEAREST_NEIGHBOR) = 0;
  virtual void SetDataPoint(const glm::vec3& pos, T data) = 0;
};

}  // namespace vq

#endif  // LIBRARY_VQ_DATA_PROVIDER_HPP_
