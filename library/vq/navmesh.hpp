//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_VQ_NAVMESH_HPP_
#define LIBRARY_VQ_NAVMESH_HPP_

#include <glm/glm.hpp>

#include <algorithm>
#include <utility>
#include <vector>

#include "vq/export.hpp"

namespace vq {

class VQ_EXPORT Navmesh {
 public:
  Navmesh(const std::vector<glm::vec3>& vertices,
          const std::vector<glm::uvec3>& triangles);
  Navmesh() = delete;
  virtual ~Navmesh() = default;
  Navmesh(const Navmesh&) = default;
  Navmesh(Navmesh&&) = default;

  Navmesh& operator=(const Navmesh&) = default;
  Navmesh& operator=(Navmesh&&) = default;

  inline glm::vec3 GetVertex(unsigned int vertex_idx) const {
    return vertices_[vertex_idx];
  }
  glm::uvec3 GetTriangle(unsigned int triangle_idx) const {
    return triangles_[triangle_idx];
  }
  inline std::size_t GetNumVertices() const { return vertices_.size(); }
  inline std::size_t GetNumTriangles() const { return triangles_.size(); }

  bool GetEdgeExists(unsigned int vertex_idx1, unsigned int vertex_idx2);
  const std::vector<unsigned int>& GetNeighbors(unsigned int vertex_idx);
  const std::vector<unsigned int>& GetIncidentTriangles(
      unsigned int vertex_idx);
  std::vector<unsigned int> GetIncidentTriangles(unsigned int vertex_idx1,
                                                 unsigned int vertex_idx2);

  float GetTriangleArea(unsigned int triangle_idx) const;
  bool GetTriangleBelowPosition(
      const glm::vec3& pos,
      unsigned int& triangle_idx,             // NOLINT
      glm::vec3& projected_pos_on_triangle);  // NOLINT
  bool GetClosestTriangleEdgeBelowPosition(
      const glm::vec3& pos,
      unsigned int& triangle_idx,            // NOLINT
      glm::vec3& projected_pos_on_triangle,  // NOLINT
      float max_distance = 25.f);
  bool GetClosestVertexOfClosestTriangleBelowPosition(
      const glm::vec3& pos, unsigned int& vertex_idx,  // NOLINT
      float max_check_distance = 25.f);

  std::pair<glm::vec3, glm::vec3> GetBounds() const { return bounds_; }

 private:
  void BuildAdjacencyStructure();
  void ComputeBounds();

  void BuildAccelerationStructure();
  std::vector<std::size_t> GetAccGridIndicesAlongEdge(const glm::vec3& start,
                                                      const glm::vec3& end);
  void InsertEdgeIntoAccGrid(unsigned int uiStartVertex,
                             unsigned int uiEndVertex);
  void InsertTriangleIdxIntoAccGrid(std::size_t grid_index,
                                    unsigned int triangle_idx);
  glm::uvec4 GetTriangleAccGridBoundingBox(unsigned int triangle_idx) const;
  void InsertTriangleIntoAccGridCellsTotallyCoveredByIt(
      unsigned int triangle_idx);
  inline std::size_t GetAccGridIndex(unsigned int x, unsigned int z) const {
    return z * acc_grid_dims_[0] + x;
  }
  inline std::size_t GetAccGridIndex(const glm::uvec2& coords) const {
    return coords[1] * acc_grid_dims_[0] + coords[0];
  }
  inline glm::uvec2 GetAccGridCoords(const glm::vec3& pos) const {
    return glm::uvec2(
        static_cast<unsigned int>(std::max(
            0.f,
            std::floor((pos[0] - bounds_.first[0]) / acc_grid_sidelength_))),
        static_cast<unsigned int>(std::max(
            0.f,
            std::floor((pos[2] - bounds_.first[2]) / acc_grid_sidelength_))));
  }

  std::vector<glm::vec3> vertices_;
  std::vector<glm::uvec3> triangles_;

  bool adjacency_structure_dirty_ = true;
  std::vector<std::vector<unsigned int>> edges_;
  std::vector<std::vector<unsigned int>> incident_triangles_;

  bool acceleration_structure_dirty_ = true;
  glm::uvec2 acc_grid_dims_;
  float acc_grid_sidelength_ = 1.f;
  std::vector<std::vector<unsigned int>> acc_grid_triangles_by_cell_;
  std::vector<std::vector<std::size_t>> acc_grid_cells_by_triangle_;

  std::pair<glm::vec3, glm::vec3> bounds_;
};

}  // namespace vq

#endif  // LIBRARY_VQ_NAVMESH_HPP_
