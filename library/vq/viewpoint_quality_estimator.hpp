//------------------------------------------------------------------------------
// Viewpoint Quality Lib
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_VQ_VIEWPOINT_QUALITY_ESTIMATOR_HPP_
#define LIBRARY_VQ_VIEWPOINT_QUALITY_ESTIMATOR_HPP_

#include <vector>

#include "vq/export.hpp"
#include "vq/visibility_histogram.hpp"

namespace vq {

class VQ_EXPORT ViewpointQualityEstimator {
 public:
  ViewpointQualityEstimator() = default;
  virtual ~ViewpointQualityEstimator() = default;
  ViewpointQualityEstimator(const ViewpointQualityEstimator&) = default;
  ViewpointQualityEstimator(ViewpointQualityEstimator&&) = default;

  ViewpointQualityEstimator& operator=(const ViewpointQualityEstimator&) =
      default;
  ViewpointQualityEstimator& operator=(ViewpointQualityEstimator&&) = default;

  virtual float GetViewpointQuality(const VisibilityHistogram* visibility) = 0;

  void SetWeight(unsigned int entity_idx, float weight);
  float GetWeight(unsigned int entity_idx) const;
  void ResetWeights();

 protected:
  std::vector<float> weights_;
};

}  // namespace vq

#endif  // LIBRARY_SRC_VISIBILITY_HISTOGRAM_HPP_
