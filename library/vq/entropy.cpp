//------------------------------------------------------------------------------
// Viewpoint Quality
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "entropy.hpp"

#include <cmath>

namespace vq {
float Entropy::GetViewpointQuality(const VisibilityHistogram* visibility) {
  if (visibility == nullptr) return 0.f;

  float total_entropy = 0.f;

  for (const auto& entry : *visibility) {
    if (entry.first > 0 || include_background_) {
      float ai_at = entry.second;
      if (ai_at > 0.f)
        total_entropy += ai_at * std::log2(ai_at) * GetWeight(entry.first);
    }
  }

  total_entropy *= -1.f;
  if (exponential_) total_entropy = std::pow(2.f, total_entropy) - 1.f;
  return total_entropy;
}

void Entropy::SetIncludeBackground(bool include_background) {
  include_background_ = include_background;
}

bool Entropy::GetIncludeBackground() const { return include_background_; }

void Entropy::SetExponential(bool exponential) { exponential_ = exponential; }

bool Entropy::GetExponential() const { return exponential_; }

}  // namespace vq
