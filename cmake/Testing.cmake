#------------------------------------------------------------------------------
# Viewpoint Quality Lib
#
# Copyright (c) 2017-2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
#------------------------------------------------------------------------------
#                                 License
#
# Licensed under the 3-Clause BSD License (the "License");
# you may not use this file except in compliance with the License.
# See the file LICENSE for the full text.
# You may obtain a copy of the License at
#
#     https://opensource.org/licenses/BSD-3-Clause
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------------------------

include(CTest)
enable_testing()

include(WarningLevels)

conan_or_find_package(catch REQUIRED)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

set_property(GLOBAL PROPERTY ADDED_TESTS "")


function(CONFIGURE_MSVC_USERFILE TARGET_NAME PATH_TO_ADD)
  file(TO_NATIVE_PATH "${PATH_TO_ADD}/Release" _DLL_PATH_RELEASE)
  file(TO_NATIVE_PATH "${PATH_TO_ADD}/Debug" _DLL_PATH_DEBUG)
  set(SOURCE_USER_FILE
    "${CMAKE_SOURCE_DIR}/cmake/VisualStudio2013.vcxproj.user.in")
  set(DESTINATION_USER_FILE
    "${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}.vcxproj.user")
  configure_file(${SOURCE_USER_FILE} ${DESTINATION_USER_FILE} @ONLY)
endfunction()

function(ADD_TEST_TARGET_INTERNAL_
    NAME SOURCES HEADERS INCLUDE_DIRECTORIES LINK_LIBRARIES PATH_TO_ADD)
  add_executable(${NAME} ${SOURCES} ${HEADERS})
  target_include_directories(${NAME} PRIVATE ${INCLUDE_DIRECTORIES})
  target_link_libraries(${NAME} ${LINK_LIBRARIES})
  target_link_libraries(${NAME} ${CONAN_OR_CMAKE_catch})
   
  if(WIN32 AND MSVC)
    CONFIGURE_MSVC_USERFILE(${NAME} ${PATH_TO_ADD})
  endif()
endfunction()

function(ADD_TEST_CATCH_INTERNAL_ 
    NAME SOURCES HEADERS)
  
  add_test(NAME ${NAME} COMMAND ${NAME})
  if(NOT ${NAME} MATCHES "integration")
    set_tests_properties(${NAME} PROPERTIES TIMEOUT 10.0)
  else()
    set_tests_properties(${NAME} PROPERTIES TIMEOUT 120.0)
  endif()
  set_warning_levels_RWTH(${NAME})

  set_property(TARGET ${NAME} PROPERTY FOLDER "Tests")
  source_group("Source Files" FILES ${SOURCES} ${HEADERS})
  
  set_property(GLOBAL APPEND PROPERTY ADDED_TESTS "${NAME}")
  
  if(IS_BUILD_SERVER)
    target_compile_definitions(${NAME} PUBLIC -DIS_BUILD_SERVER)
  endif()
  
endfunction()

function(ADD_TEST_INTERNAL_ 
    NAME SOURCES HEADERS INCLUDE_DIRECTORIES LINK_LIBRARIES PATH_TO_ADD)
  ADD_TEST_TARGET_INTERNAL_("${NAME}" "${SOURCES}" "${HEADERS}" "${INCLUDE_DIRECTORIES}" "${LINK_LIBRARIES}" "${PATH_TO_ADD}")
  ADD_TEST_CATCH_INTERNAL_("${NAME}" "${SOURCES}" "${HEADERS}")
endfunction()




function(CREATE_CATCH_MAIN_INTERNAL_
    NAME SOURCE INCLUDE_DIRECTORIES)
  add_library(${NAME} ${SOURCE})
  target_include_directories(${NAME} PRIVATE ${INCLUDE_DIRECTORIES})
  target_link_libraries(${NAME} ${CONAN_OR_CMAKE_catch} )
  set_property(TARGET ${NAME} PROPERTY FOLDER "Tests")
  source_group("Source Files" FILES ${SOURCE})
endfunction()


function(ADD_TEST_CATCH)
  # parse arguments
  set(options )
  set(oneValueArgs NAME CATCH_MAIN)
  set(multiValueArgs
    SOURCES HEADERS INCLUDE_DIRECTORIES LINK_LIBRARIES PATH_TO_ADD)
  cmake_parse_arguments(ADD_TEST_CATCH
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  create_catch_main_internal_("${ADD_TEST_CATCH_NAME}_catch_main"
    ${ADD_TEST_CATCH_CATCH_MAIN}
    ${ADD_TEST_CATCH_INCLUDE_DIRECTORIES}
  )

  # remove catch_main file from sources
  file(GLOB ADD_TEST_CATCH_CATCH_MAIN_ABSOLUTE ${ADD_TEST_CATCH_CATCH_MAIN})
  list(REMOVE_ITEM ADD_TEST_CATCH_SOURCES ${ADD_TEST_CATCH_CATCH_MAIN_ABSOLUTE})

  # add test for each test source file
  foreach(TEST_SOURCE_FILE ${ADD_TEST_CATCH_SOURCES})
    get_filename_component(TEST_NAME ${TEST_SOURCE_FILE} NAME_WE)
    ADD_TEST_INTERNAL_("${TEST_NAME}"
      "${TEST_SOURCE_FILE}"
      ""
      "${ADD_TEST_CATCH_INCLUDE_DIRECTORIES}"
      "${ADD_TEST_CATCH_LINK_LIBRARIES};${ADD_TEST_CATCH_NAME}_catch_main"
      "${ADD_TEST_CATCH_PATH_TO_ADD}"
    )
  endforeach()
endfunction()

macro(EXTRACT_UNIT_AND_INTEGRATION_TEST_TARGETS)
  get_property(added_tests GLOBAL PROPERTY ADDED_TESTS)
  foreach(filename ${added_tests})
  	if(${filename} MATCHES "integration_test")
  	  message(STATUS "Integration Test: ${filename}")
  	  set(INTEGRATION_TEST_TARGETS ${INTEGRATION_TEST_TARGETS} ${filename})
  	else()
  	  message(STATUS "Unit Test: ${filename}")
  	  set(UNIT_TEST_TARGETS ${UNIT_TEST_TARGETS} ${filename})
  	endif()
  endforeach()
endmacro()

macro(ADD_TEST_SUITE)
  set(options)
  set(oneValueArgs NAME TEST_REGEX)
  set(multiValueArgs DEPEND_ON_TARGETS)
  cmake_parse_arguments(ADD_TEST_SUITE
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
  #we have to escape the " for visual studio)
  add_custom_target(${ADD_TEST_SUITE_NAME} COMMAND ctest --tests-regex \"${ADD_TEST_SUITE_TEST_REGEX}\" -C $<CONFIG>)
   # This comment just ends the escaped signs, for VS highlighting "
  if(ADD_TEST_SUITE_DEPEND_ON_TARGETS)
    add_dependencies(${ADD_TEST_SUITE_NAME} ${ADD_TEST_SUITE_DEPEND_ON_TARGETS})
  endif()
  set_property(TARGET ${ADD_TEST_SUITE_NAME} PROPERTY FOLDER "_Test-Suites_")
endmacro()



function(CREATE_TEST_SUITES)
	EXTRACT_UNIT_AND_INTEGRATION_TEST_TARGETS()

	ADD_TEST_SUITE(
	  NAME "Unit-Test-Suite"
	  TEST_REGEX "^test"
	  DEPEND_ON_TARGETS ${UNIT_TEST_TARGETS})

	ADD_TEST_SUITE(
	  NAME "Integration-Test-Suite"
	  TEST_REGEX "^integration"
	  DEPEND_ON_TARGETS ${INTEGRATION_TEST_TARGETS})
	
	ADD_TEST_SUITE(
	  NAME "Cpplint-Test-Suite"
	  TEST_REGEX "cpplint")

	ADD_TEST_SUITE(
	  NAME "Cppcheck-Test-Suite"
	  TEST_REGEX "cppcheck")
endfunction()
